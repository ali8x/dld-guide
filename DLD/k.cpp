
#include <iostream>
#include <string>
#include "math.h"
#include "conio.h"
#include "stacko.h"
#include <vector>
#include <stack>
#include <set>
#include <map>
#include <math.h>

using namespace std;

struct pointXY {
public:
	int x;
	int y;
	bool flag;
	pointXY() {
		x=0;
		y=0;
		flag = false;
	}
	pointXY(int mX,int mY) {
		x = mX;
		y = mY;
		flag = false;
	}
    bool operator==(const pointXY& rhs) const {
        return ((rhs.x==x)&&(rhs.y==y));
    }
    bool operator!=(const pointXY& rhs) const {
        return !((rhs.x==x)&&(rhs.y==y));
    }
    bool operator<(const pointXY& rhs) const {
		if( x < rhs.x ) return true;
		if( x > rhs.x ) return false;
		return y < rhs.y;

    }
};

struct kMap {
	map<char,int> varsMap;
	vector<bool> res;
	vector<vector<bool>> kmap;
	vector<vector<bool>> dmap;
	vector<vector<int>> imap;
	vector<vector<bool>> truthTable;
	vector<string> gW;
	vector<string> gH;
	int w,h,mSize;
	vector<string> strPIs;
	vector<string> strEPIs;
	string PITerms;
	string EPITerms;
	vector<vector<pointXY>> PIs;
	vector<vector<pointXY>> EPIs;
};

	string solveKMap(kMap & k1) {
		int size = k1.res.size();
		string PITerms;
		string EPITerms;
		vector<vector<pointXY>> PIs;	// storing all prime implicants groups
		vector<vector<pointXY>> EPIs;	// storing all essential prime implicants groups
		vector<string> strPIs;
		vector<string> strEPIs;			// storing EPI terms

		for(int s = k1.mSize; s > 0 ; s=s/2) {

			set<pointXY> groups = generateGroups(s,k1.w,k1.h);

			for(int x = 0; x < k1.w ; x++) {
				for(int y = 0; y < k1.h ; y++) {

					for (set<pointXY>::iterator iter = groups.begin(); iter != groups.end(); ++iter) {

						// -start group
						bool allOnes = true;
						bool allDone = true;
						for(int i = x; i < x+iter->x ; i++) {
							for(int j = y; j < y+iter->y ; j++) {
								int cX = i%k1.w; //circular X
								int cY = j%k1.h; //circular Y
								if(k1.kmap[cX][cY] == false) {
									allOnes = false;
								}
								if(k1.dmap[cX][cY] == false) {
									allDone = false;
								}
							}
						}

						if(allOnes == true && allDone == false) {
							
							for(int i = x; i < x+iter->x ; i++) {
								for(int j = y; j < y+iter->y ; j++) {
									int cX = i%k1.w;
									int cY = j%k1.h;
									k1.dmap[cX][cY] = true;	
									group.push_back(pointXY(cX,cY));
								}
							}
							PIs.push_back(group);
							
							vector<string> greysY;
							for(int j = y; j < y+iter->y ; j++) {
								int cY = j%k1.h; //circular Y
								string gW = k1.gH[cY];
								greysY.push_back(gW);
							}
							string yVars = getKMapVars(greysY,k1.varsMap,0);

							vector<string> greysX;
							for(int i = x; i < x+iter->x ; i++) {
								int cX = i%k1.w; //circular X
								string gH = k1.gW[cX];
								greysX.push_back(gH);
							}
							string xVars = getKMapVars(greysX,k1.varsMap,twoInv(k1.h));

							string PITerm = yVars + xVars;
							PITerms = PITerm + "+" + PITerms;

							strPIs.push_back(yVars + xVars);

						}

						//_getch();
					}

				}
			}
		}
