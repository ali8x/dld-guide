#include <string>
using namespace std;

#pragma once
class stacko
{
public:
	stacko(void) {
		top = -1;
		max_items = 200;
		arr = new string[max_items];
	}

	void push(string c) {
		top++;
		arr[top] = c;
	}

	string pop() {
		string c = arr[top];
		top--;
		return c;
	}

	string topVal() {
		string c = arr[top];
		return c;
	}

	bool isEmpty() {
		return top == -1;
	}

	int size() {
		return top+1;
	}

	~stacko(void) {

	}
private:
	string * arr;
	int max_items;
	int top;
};

