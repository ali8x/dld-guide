#include "stdafx.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "math.h"
#include "conio.h"
#include "stacko.h"
#include <vector>
#include <stack>
#include <functional>
#include <set>
#include <map>
#include <math.h>

using namespace std;

struct pointXY {
public:
	int x;
	int y;
	bool flag;
	pointXY() {
		x=0;
		y=0;
		flag = false;
	}
	pointXY(int mX,int mY) {
		x = mX;
		y = mY;
		flag = false;
	}
    bool operator==(const pointXY& rhs) const {
        return ((rhs.x==x)&&(rhs.y==y));
    }
    bool operator!=(const pointXY& rhs) const {
        return !((rhs.x==x)&&(rhs.y==y));
    }
    bool operator<(const pointXY& rhs) const {
		if( x < rhs.x ) return true;
		if( x > rhs.x ) return false;
		return y < rhs.y;

    }
};

struct lineXY {
public:
	int x1;
	int y1;
	int x2;
	int y2;
	bool flag;
	lineXY() {
		x1=0;
		y1=0;
		x2=0;
		y2=0;
		flag = false;
	}
	lineXY(int mX1,int mY1, int mX2, int mY2) {
		x1 = mX1;
		y1 = mY1;
		x2 = mX2;
		y2 = mY2;
		flag = false;
	}
    bool operator==(const lineXY& rhs) const {
        return ((rhs.x1==x1)&&(rhs.y1==y1)&&(rhs.x2==x2)&&(rhs.y2==y2));
    }
    bool operator!=(const lineXY& rhs) const {
        return !((rhs.x1==x1)&&(rhs.y1==y1)&&(rhs.x2==x2)&&(rhs.y2==y2));
    }
    bool operator<(const lineXY& rhs) const {
		if( x1 < rhs.x1 ) return true;
		if( x1 > rhs.x1 ) return false;

		if( y1 < rhs.y1 ) return true;
		if( y1 > rhs.y1 ) return false;

		if( x2 < rhs.x2 ) return true;
		if( x2 > rhs.x2 ) return false;

		if( y2 < rhs.y2 ) return true;
		if( y2 > rhs.y2 ) return false;

		return y1 < rhs.y1;
    }
};

struct kMap {
	map<char,int> varsMap;
	vector<bool> res;
	vector<vector<bool>> kmap;
	vector<vector<bool>> dmap;
	vector<vector<int>> imap;
	vector<vector<bool>> truthTable;
	vector<string> gW;
	vector<string> gH;
	int w,h,mSize;
	vector<string> strPIs;
	vector<string> strEPIs;
	string PITerms;
	string EPITerms;
	vector<vector<pointXY>> PIs;
	vector<vector<pointXY>> EPIs;
};

class BinaryNode {
public:
	string info;
	BinaryNode * left;
	BinaryNode * right;
	pointXY position;
	BinaryNode() {
		left = NULL;
		right = NULL;
		info = "";
	}
	BinaryNode(string m_info, BinaryNode * m_left = NULL, BinaryNode * m_right = NULL) {
		info = m_info;
		left = m_left;
		right = m_right;
	}
};

class BinaryTree {
	
	void cleanup() {
		cleanup_r(root);
	}
	void cleanup_r(BinaryNode * m_root) {
		if(m_root==NULL) {
			return;
		}
		cleanup_r(m_root->left);
		cleanup_r(m_root->right);
		delete m_root;
	}
public:
	BinaryNode * root;
	BinaryTree () {
		root = NULL;
	}
	BinaryTree (BinaryNode * mRoot) {
		root = mRoot;
	}
	~BinaryTree () {
		cleanup();
	}
	
	void infixSurf(const function<void (int,BinaryNode*)>& f) {
		infixSurf(root,0,f);
	}

	void infixSurf(BinaryNode * m_root,int level, const function<void (int,BinaryNode*)>& f) {
		if(m_root==NULL) {
			return;
		}
		infixSurf(m_root->left,level+1,f);
		f(level,m_root);
		infixSurf(m_root->right,level+1,f);

	}
	
	void prefixSurf(const function<void (int,BinaryNode*)>& f) {
		prefixSurf(root,0,f);
	}

	void prefixSurf(BinaryNode * m_root,int level, const function<void (int,BinaryNode*)>& f) {
		if(m_root==NULL) {
			return;
		}
		prefixSurf(m_root->left,level+1,f);
		prefixSurf(m_root->right,level+1,f);
		f(level,m_root);

	}

	void postfixSurf(const function<void (int,BinaryNode*)>& f) {
		postfixSurf(root,0,f);
	}

	void postfixSurf(BinaryNode * m_root,int level, const function<void (int,BinaryNode*)>& f) {
		if(m_root==NULL) {
			return;
		}
		f(level,m_root);
		postfixSurf(m_root->left,level+1,f);
		postfixSurf(m_root->right,level+1,f);
		
	}

	void getMap(vector<BinaryNode *> & vct) {
		int y = 0;
		infixSurf([&y] (int level, BinaryNode * node) {
			node->position = pointXY(level, y);
			y++;
		});
		getMap(root,0,vct);

		int xMax = getXMax();

		postfixSurf([&] (int level, BinaryNode * node) {
			int m_xMax = getXMax(node);
			if(m_xMax < xMax) {
				int diff = xMax-m_xMax;

				infixSurf(node, node->position.x, [&diff] (int m_level, BinaryNode * m_node) {
					m_node->position.x += diff;
				});

			}
		});

	}

	void getMap(BinaryNode * m_root, int level, vector<BinaryNode *> & vct) {
		if(m_root==NULL) {
			return;
		}
		getMap(m_root->left,level+1,vct);
		vct.push_back(m_root);
		getMap(m_root->right,level+1,vct);
	}
	
	int getXMax() {
		int max = 0;
		infixSurf([&max] (int level, BinaryNode * node) {
			if (node->position.x > max) {
				max = node->position.x;
			}
		});
		return max;
	}
	int getXMax(BinaryNode * m_root) {
		int max = 0;
		infixSurf(m_root,0,[&max] (int level, BinaryNode * node) {
			if (node->position.x > max) {
				max = node->position.x;
			}
		});
		return max;
	}
	int getYMax() {
		int max = 0;
		infixSurf([&max] (int level, BinaryNode * node) {
			if (node->position.y > max) {
				max = node->position.y;
			}
		});
		return max;
	}

	int getNumLeavesJoints() {
		int maxX = getXMax();
		int l = 0;
		infixSurf([&maxX,&l] (int level, BinaryNode * node) {
			
			if(node->left!=0 && node->left->position.x == maxX) {
				l++;
			}
			if(node->right!=0 && node->right->position.x == maxX) {
				l++;
			}
		});
		return l;
	}

	string getExpression(BinaryNode * m_root) {
		string str;

		getExpression_r(m_root,str);

		return str;
	}

	void getExpression_r(BinaryNode * m_root, string & str) {

		if(m_root==NULL) {
			return;
		}

		if(m_root->left == 0 && m_root->right == 0) {
			str += m_root->info;
		} else {
			str += "(";
			if(m_root->info == "'") {
				getExpression_r(m_root->left,str);
				str += m_root->info;
			} else {
				getExpression_r(m_root->right,str);
				str += m_root->info;
				getExpression_r(m_root->left,str);
			}
			str += ")";
		}

	}

};


class DLDS {
	//Postfix Evaluator

private:

	// private parts
	string * operatorsArr;
	int operatorsCount;

	// get precedence of operator
	int getPrecedence(string str) {
		for(int i = 0; i < operatorsCount; i++) {
			if(operatorsArr[i] == str) {
				return i;
			}
		}
		return -1;
	}

	// parse 2 #s based on symbol
	bool parseRes(bool r1, bool r2, string op) {
		if(op == "+") {
			return (r1 || r2);
		} else if(op == "*") {
			return (r1 && r2);
		} else if(op == "^") {
			return (r1 != r2);
		}
		return 0;
	}

public:

	// construct & initialize
	DLDS() {
		//string lst [] = {"+","-","*","/","%","^","$"};
		string lst [] = {"+","^","*"};
		operatorsCount = sizeof(lst)/sizeof(lst[0]);
		operatorsArr = new string[operatorsCount];
		for(int i = 0; i< operatorsCount; i++) {
			operatorsArr[i] = lst[i];
		}
	}


	// check if it's an operator
	bool isOperator(string str) {
		for(int i = 0; i < operatorsCount; i++) {
			if(operatorsArr[i] == str) {
				return true;
			}
		}
		return false;
	}

	bool isOperand(string str) {
		char c = str[0];
		if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
			return true;
		}
		return false;
	}

	// convert expresson to postfix
	string toPostFix(string eq) {
		string nstr;
		stacko st;

		for(int i = 0; i< eq.length(); i++) {
			string sop = string(&eq[i],1);
			if(sop == "(") {
				if(i > 0) {
					string tvop = string(&eq[i-1],1);
					if(isOperand(tvop) || tvop == ")" || tvop == "'") {
						st.push("*");
					}
				}
				st.push(sop);
			} else if(sop == " " || sop == "\t" || sop == "\n" || sop == "\r") {
			} else if(sop == ")") {
				while(!st.isEmpty()) {
					string fnd = st.pop();
					if(fnd == "(") {
						break;
					} else {
						nstr += " " + fnd;
					}
				}
			} else if(isOperator(sop)) {
				int p = getPrecedence(sop);
				if(st.size() > 0) {
					string temp = st.pop();
					int p_temp = getPrecedence(temp);
					if(p_temp >= p) {
						nstr += " " + temp;
						while(!st.isEmpty()) {
							string fnd = st.pop();
							if(fnd == "(") {
								st.push(fnd);
								break;
							} else if(getPrecedence(fnd) < p_temp) {
								st.push(fnd);
								break;
							} else {
								nstr += " " + fnd;
							}
						}
						st.push(sop);
					} else {
						st.push(temp);
						st.push(sop);
					}
				} else {
					st.push(sop);
				}
			} else if(sop == "'") {
				nstr += " " + sop;
			} else if(isOperand(sop)) {
				if(i > 0) {
					string tvop = string(&eq[i-1],1);
					if(isOperand(tvop) || tvop == ")" || tvop == "'") {
						st.push("*");
					}
				}
				nstr += " " + sop;
			} else {
				nstr += " " + sop;
			}
		}

		while(!st.isEmpty()) {
			nstr += " " + st.pop();
		}
		nstr = nstr.substr(1,nstr.size()-1);

		return nstr;
	}

	// solve the given postfix
	double long solvePostFix(string eq, map<char,bool> valsMap) {
		stacko st;

		for(int i = 0; i< eq.length(); i++) {
			string sop = string(&eq[i],1);
			if(isOperator(sop)) {
				string n2 = st.pop();
				string n1 = st.pop();
				bool r1 = false;
				bool r2 = false;
				

				if(isOperand(n2)) {
					r2 = valsMap[n2[0]];
				} else {
					r2 = !(n2 == "0");
				}

				if(isOperand(n1)) {
					r1 = valsMap[n1[0]];
				} else {
					r1 = !(n1 == "0");
				}

				bool res = parseRes(r1,r2,sop);

				stringstream ss; ss << res;
				string sres = ss.str();
				st.push(sres);

			} else if(eq[i] == ' ') {
			
			} else if(eq[i] == '\'') {

				string n2 = st.pop();
				bool r2 = false;
				if(isOperand(n2)) {
					r2 = valsMap[n2[0]];
				} else {
					r2 = !(n2 == "0");
				}

				r2 = !r2;
				stringstream ss; ss << r2;
				string sres = ss.str();
				st.push(sres);

			} else {
				st.push(sop);
			}
		}
		string nstr = st.pop();
		bool r;
		if(isOperand(nstr)) {
			r = valsMap[nstr[0]];
		} else {
			r = !(nstr == "0");
		}

		return r;
	}
	
	BinaryTree toBinaryExpressionTree(string postfix) {
		stack<BinaryNode *> st;

		for(int i = 0; i< postfix.length(); i++) {
			string sop = string(&postfix[i],1);
			if(isOperator(sop)) {
				BinaryNode * l = st.top();
				st.pop();
				BinaryNode * r = st.top();
				st.pop();
				BinaryNode * n = new BinaryNode(sop, l, r);
				st.push(n);
			} else if(postfix[i] == ' ') {
			} else if(postfix[i] == '\'') {
				BinaryNode * l = st.top();
				st.pop();
				BinaryNode * n = new BinaryNode("'", l);
				st.push(n);
			} else {
				st.push(new BinaryNode(sop));
			}
		}
		
		return st.top();
	}


	vector<string> splitEx(string eqs) {
		vector<string> v;

		string neqs;

		for(int i = 0; i < eqs.size(); i++) {
			if(eqs[i] == ' ' || eqs[i] == '\t' || eqs[i] == '\n' || eqs[i] == '\r') {
			} else {
				neqs += eqs[i];
			}
		}

		stringstream ss;
		ss << neqs;

		string eq;
		while(getline(ss,neqs,';')) {
			v.push_back(neqs);
		}

		return v;
	}


	bool hasVars(string eq) {
		bool r = false;

		string neqs;

		for(int i = 0; i < eq.size(); i++) {
			string sop = string(&eq[i],1);
			if(isOperand(sop) == true) {
				r = true;
			}
		}

		return r;
	}

	map<char,int> getVarsMap(string eq) {
		map<char,int> lee;
		for(int i = 0; i< eq.length(); i++) {
			string sop = string(&eq[i],1);
			if(isOperand(sop)) {
				lee[eq[i]] = 0;
			}
		}
		map<char,int>::iterator iter;
		int i = 0;
		for (iter = lee.begin(); iter != lee.end(); ++iter) {
			iter->second = i;
			i++;
		}
		return lee;
	}

	vector<vector<bool>> getTruthTable(map<char,int> lee) {
		vector<vector<bool>> ret;
		int k = pow(2,(double)lee.size());

		for(int i = lee.size()-1; i >= 0; i--) {
			bool x = false;
			int j = pow(2,(double)i);
			vector<bool> sret;

			for(int l = 0; l < k ; l++) {
				if(l%j == 0 && l > 0) {
					x = !x;
				}
				sret.push_back(x);
			}
			ret.push_back(sret);
		}
		return ret;
	}

	vector<vector<bool>> getFunctVals(vector<vector<bool>> tt, map<char,int> varsMap, vector<string> equations) {

		vector<vector<bool>> ret;

		for(int i = 0 ; i < equations.size() ; i++) {
			if(equations[i]=="") { continue; }
			string pfn = toPostFix(equations[i]);

			vector<bool> results;

			vector<bool> vb = tt[0];
			map<char,int>::iterator iter;

			for(int j = 0 ; j < vb.size() ; j++) {
				map<char,bool> valsMap;

				for (iter = varsMap.begin(); iter != varsMap.end(); ++iter) {
					valsMap[iter->first] = tt[iter->second][j];
				}
				bool b = solvePostFix(pfn, valsMap);
				results.push_back(b);
			}

			ret.push_back(results);
		}

		return ret;
	}
	
	string toSOP(vector<bool> res) {
		stringstream ss;
		ss << "S(";
		for(int j = 0 ; j < res.size() ; j++) {
			if(res[j] == true) {
				ss << j << ',';
			}
		}
		string str = ss.str();
		if(str.length()>0) {
			str = str.substr(0,str.size()-1);
		}
		str+= ')';
		return str;
	}

	string toPOS(vector<bool> res) {
		stringstream ss;
		ss << "P(";
		for(int j = 0 ; j < res.size() ; j++) {
			if(res[j] == false) {
				ss << j << ',';
			}
		}
		string str = ss.str();
		if(str.length()>0) {
			str = str.substr(0,str.size()-1);
		}
		str+= ')';
		return str;
	}

	string toSOPx(vector<vector<bool>> tt, map<char,int> varsMap, vector<bool> res) {
		stringstream ss;

		for(int j = 0 ; j < res.size() ; j++) {
			if(res[j] == true) {
				
				map<char,int>::iterator iter;
				for (iter = varsMap.begin(); iter != varsMap.end(); ++iter) {
					bool val = tt[iter->second][j];
					if(val == true) {
						ss << iter->first;
					} else {
						ss << iter->first << '\'';
					}
				}
				ss << '+';

			}
		}

		string str = ss.str();
		if(str.length()>0) {
			str = str.substr(0,str.size()-1);
		}

		return str;
	}

	string toPOSx(vector<vector<bool>> tt, map<char,int> varsMap, vector<bool> res) {
		stringstream ss;

		for(int j = 0 ; j < res.size() ; j++) {
			if(res[j] == false) {
				
				ss << '(';
				map<char,int>::iterator iter;
				int i = 0;
				for (iter = varsMap.begin(); iter != varsMap.end(); ++iter) {
					bool val = tt[iter->second][j];
					if(val == false) {
						ss << iter->first;
					} else {
						ss << iter->first << '\'';
					}
					if(i<tt.size()-1) {
						ss << '+';
					}
					i++;
				}
				ss << ')';

			}
		}

		return ss.str();
	}

	string generateKMapStr (kMap k1) {
		string str;
		
		int w = k1.kmap[0].size();
		int h = k1.kmap.size();

		for(int i = 0; i<w; i++) {
			for(int j = 0; j<h; j++) {
				str += string("\t") + (k1.kmap[j][i]?"1":"0");
			}
			str += "\n\n";
		}
		return str;
	}

	kMap generateKMap(vector<vector<bool>> truthTable, map<char,int> varsMap, vector<bool> res) {
		kMap k1;
		int size = res.size();
		int w = 0;
		int h = 0;
		pointXY psize;
		
		psize = generateKSize(size);

		if(size >= 4) {
			w = psize.x;
			h = psize.y;
		} else {
			throw("Size not supported for K-Map");
		}

		int mSize = w*h;
		
		vector<string> gW = generateGreys(w);
		vector<string> gH = generateGreys(h);

		k1.truthTable = truthTable;
		k1.res = res;
		k1.varsMap = varsMap;
		k1.w = w;
		k1.h = h;
		k1.mSize = mSize;
		k1.gW = gW;
		k1.gH = gH;

		for(int x = 0; x < w; x++) {
			vector<bool> m_kmap;
			vector<bool> m_dmap;
			vector<int> m_imap;
				for(int y = 0; y < h; y++) {
				string str = gH[y] + gW[x];
				int n = binToDec(str);
				m_imap.push_back(n);
				bool b = res[n];
				m_kmap.push_back(b);
				m_dmap.push_back(false);
			}
			k1.kmap.push_back(m_kmap);
			k1.dmap.push_back(m_dmap);
			k1.imap.push_back(m_imap);
		}
		return k1;
	}

	pointXY generateKSize(int size) {
		pointXY pt;
		int maxDist = 0;
		for(int x = size; x > 0 ; x=x/2) {
			for(int y = size; y > 0 ; y=y/2) {
				if(x==y && x*y == size) {
					return pointXY(x,y);
				} else if(x*y == size) {
					int dist = x-y;
					if(dist>maxDist) {
						dist = maxDist;
						pt = pointXY(x,y);
					}
				}
			}
		}
		return pt;
	}
	
	set<pointXY> generateGroups(int size, int w, int h) {
		set<pointXY> st;
		for(int x = size; x > 0 ; x=x/2) {
			for(int y = size; y > 0 ; y=y/2) {
				if(x*y == size) {
					if(x<=w && y<=h) {
						st.insert(pointXY(x,y));
					}
				}
			}
		}
		return st;
	}

	string solveKMap(kMap & k1) {
		int size = k1.res.size();
		string PITerms;
		string EPITerms;
		vector<vector<pointXY>> PIs;	// storing all prime implicants groups
		vector<vector<pointXY>> EPIs;	// storing all essential prime implicants groups
		vector<string> strPIs;
		vector<string> strEPIs;			// storing EPI terms

		// check all sets : 16, 8, 4, 2, 1
		for(int s = k1.mSize; s > 0 ; s=s/2) {

			// make groups for sizes: 16->4,4
			set<pointXY> groups = generateGroups(s,k1.w,k1.h);

			// iterate through the map
			for(int x = 0; x < k1.w ; x++) {
				for(int y = 0; y < k1.h ; y++) {

					// for each element iterated, apply the groups
					for (set<pointXY>::iterator iter = groups.begin(); iter != groups.end(); ++iter) {

						// -start group
						bool allOnes = true;
						bool allDone = true;
						for(int i = x; i < x+iter->x ; i++) {
							for(int j = y; j < y+iter->y ; j++) {
								int cX = i%k1.w; //circular X
								int cY = j%k1.h; //circular Y
								if(k1.kmap[cX][cY] == false) {	// even if one is 0, allOnes = false
									allOnes = false;
								}
								if(k1.dmap[cX][cY] == false) {	// even if one is undone, allDone = false
									allDone = false;
								}
							}
						}
						// -end group

						// -now all are 1s and all are not occupied;
						if(allOnes == true && allDone == false) {
							//cout << "starts from " << x << "," << y << " of size " << iter->x << "," << iter->y << endl;
							
							vector<pointXY> group; // storing all elements of this group
							// -occupy everything;
							for(int i = x; i < x+iter->x ; i++) {
								for(int j = y; j < y+iter->y ; j++) {
									int cX = i%k1.w; //circular X
									int cY = j%k1.h; //circular Y
									k1.dmap[cX][cY] = true;		// all are now occupied;
									group.push_back(pointXY(cX,cY));
								}
							}
							PIs.push_back(group);
							// -end occupy
							
							// -finding grey-codes; going through Y
							vector<string> greysY;
							for(int j = y; j < y+iter->y ; j++) {
								int cY = j%k1.h; //circular Y
								string gW = k1.gH[cY];
								greysY.push_back(gW);
							}
							// -Y codes done
							string yVars = getKMapVars(greysY,k1.varsMap,0);
							// getting Y vars via differences


							// -finding grey-codes; going through X
							vector<string> greysX;
							for(int i = x; i < x+iter->x ; i++) {
								int cX = i%k1.w; //circular X
								string gH = k1.gW[cX];
								greysX.push_back(gH);
							}
							// -X codes done
							string xVars = getKMapVars(greysX,k1.varsMap,twoInv(k1.h));
							// getting X vars via differences

							// append the term to Prime Implicant Terms.
							string PITerm = yVars + xVars;
							PITerms = PITerm + "+" + PITerms;

							// add the term to PI vector; to sort out later on
							strPIs.push_back(yVars + xVars);

						}
						// -occupied all

						//_getch();
					}

				}
			}
		}

		// ITERATING TO FIND OUT EPIs FROM PIs
		int i = 0;
		vector<vector<pointXY>>::iterator iter;

		//set exclusions to false;
		vector<bool> exclusions;
		for (iter = PIs.begin(); iter != PIs.end(); ++iter) {
			exclusions.push_back(false);
		}

		// -go through each group; to sort out EPIs
		for (iter = PIs.begin(); iter != PIs.end(); ++iter) {
			vector<pointXY> group = *iter;

			// -go through each element; to check in other groups
			for(int j = 0; j < group.size() ; j++) {

				// -go through the next groups
				for(int k = 0; k < PIs.size() ; k++) {

					vector<pointXY> nGroup = PIs[k];

					// if its the same group ; ignore
					if(group == nGroup) {
						continue;
					}
					// if its been excluded before; ignore
					if(exclusions[k] == true) {
						continue;	
					}

					// go through each elment of next group;
					for(int l = 0; l < nGroup.size() ; l++) {
						// check if this element is the same as that element
						// if so; flag it for later checking
						if(nGroup[l] == group[j]) {
							group[j].flag = true;
						}
					}
					// -end going
				}
				// -end going
			}
			// -end

			// check all elements of the group
			// AND them; if one of them is false (not present in others); result is false.
			bool allInOthers = false;
			allInOthers = group[0].flag;
			for(int j = 1; j < group.size() ; j++) {
				allInOthers = allInOthers && group[j].flag;
			}


			if(allInOthers == true) {
				// all elements are in other groups
				// exclude this term;
				exclusions[i] = true;
			} else {
				// store that term
				EPIs.push_back(group);
				strEPIs.push_back(strPIs[i]);
				if(strPIs[i] != "") {
					EPITerms = strPIs[i] + "+" + EPITerms;
				}
			}
			i++;
		}
		// -end sorting
		
		k1.EPIs = EPIs;
		k1.PIs = PIs;

		k1.strEPIs = strEPIs;
		k1.strPIs = strPIs;

		// strip off the last +
		if(EPITerms.length()>0) {
			EPITerms = EPITerms.substr(0,EPITerms.length()-1);
		}
		if(PITerms.length()>0) {
			PITerms = PITerms.substr(0,PITerms.length()-1);
		}
		
		k1.EPITerms = EPITerms;
		k1.PITerms = PITerms;

		vector<string> eqs;
		eqs.push_back(EPITerms);

		if(EPITerms != "") {
			k1.res = getFunctVals(k1.truthTable, k1.varsMap, eqs)[0];
		}

		return EPITerms;
	}

	string getKMapVars(vector<string> greys, map<char,int> varsMap,int offset = 0) {

		string diffs = "";
		for(int j = 0; j < greys[0].size(); j++) {
			char c = '\0';
			char r = 's';
			for(int i = 0; i < greys.size(); i++) {
				if(c == '\0') {
					c = greys[i][j];
				} else {
					if(c != greys[i][j]) {
						r = 'd';
						break;
					}
				}
			}
			diffs += r;
		}

		int i = 0;
		int j = 0;
		string str = "";
		for (map<char,int>::iterator iter = varsMap.begin(); iter != varsMap.end(); ++iter) {
			if(i>=offset && i<offset+greys[0].size()) {
				if(diffs[j] == 's') {
					str += iter->first;
					if(greys[0][j] == '0') {
						str += "'";
					}
				}
				j++;
			}
			i++;
		}

		return str;
	}

	int twoInv(int n) {
		return log10((double)n)/log10((double)2);
	}

	int binToDec(string bin) {
		int s = 0;

		int l = 0;
		for(int i = bin.length()-1; i>=0 ; i--) {
			int r = bin[i]-(int)'0';
			r = r * pow((double)2,(double)l);
			l++;
			s+=r;
		}

		return s;
	}

	string XOR(string s1, string s2) {
		string str = "";
		for(int i = 0; i < s1.size(); i++) {
			if(s1[i] == s2[i]) {
				str += "0";
			} else {
				str += "1";
			}
		}
		return str;
	}

	vector<string> generateGreys(int size) {
		vector<string> ret;

		int bits = log10((double)size)/log10((double)2);
		
		for(int i = 0; i < size; i++) {
			ret.push_back("");
		}

		for(int i = 0; i < bits; i++) {
			int changeVal = pow(2,(double)(i+1));
			int midChangeVal = changeVal/2;

			bool x = false;
			int k = 1;
			for(int j = 0; j < size; j++) {

				ret[j] = (x?"1":"0") + ret[j];

				if(k==changeVal) {
					k = 1;
				} else if(k==midChangeVal) {
					x = !x;
					k++;
				} else {
					k++;
				}
			}
			ret;
		}
		return ret;
	}

	vector<lineXY> toRectLines(lineXY l, int lastLevel = 0) {
		vector<lineXY> vct;
		if(((l.x2 < l.x1)&&(l.y2 < l.y1)) || ((l.x2 < l.x1)&&(l.y2 > l.y1))) {
			// 2nd or 3rd quad;
			
			if(lastLevel == 0) {
				vct.push_back(lineXY(l.x2,l.y2,(l.x2+l.x1)/2,l.y2));				//horizontal
				vct.push_back(lineXY((l.x2+l.x1)/2,l.y2,(l.x2+l.x1)/2,l.y1));		//vertical
				vct.push_back(lineXY((l.x2+l.x1)/2,l.y1,l.x1,l.y1));				//horizontal
			} else {
				
				vct.push_back(lineXY(l.x2,l.y2,lastLevel+l.x2,l.y2));				//horizontal
				vct.push_back(lineXY(lastLevel+l.x2,l.y2,lastLevel+l.x2,l.y1));		//vertical
				vct.push_back(lineXY(lastLevel+l.x2,l.y1,l.x1,l.y1));				//horizontal
			}

		} else {
			vct.push_back(l);
		}
		return vct;
	}

}; //--