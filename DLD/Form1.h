#pragma once

#include<fstream>
#include<string>
#include<Windows.h>
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>
#include <cliext/vector>
#include <vector>
#include "DLDS.h"
#include "FrmAbout.h"

using namespace std;
using namespace System;

DLDS d1;
string Expression;
vector<string> Equations;
map<char,int> VarsMap;
vector<vector<bool>> TruthTable;
vector<vector<bool>> FunctionVals;

vector<bool> ChangedFuncs;
vector<int> AND_Count;
vector<int> OR_Count;
vector<int> NOT_Count;
vector<int> XOR_Count;



namespace DLD {

	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Drawing::Imaging;
	using namespace System::Drawing::Drawing2D;
	using namespace msclr::interop;
	using namespace System::IO;
	using namespace System::Reflection;
	using namespace System::Text;
	using System::Runtime::InteropServices::Marshal;

	static std::string toStdString(System::String^ string)
	{
		 using System::Runtime::InteropServices::Marshal;
		 System::IntPtr pointer = Marshal::StringToHGlobalAnsi(string);
		 char* charPointer = reinterpret_cast<char*>(pointer.ToPointer());
		 std::string returnString(charPointer, string->Length);
		 Marshal::FreeHGlobal(pointer);
		 return returnString;
	}
	static System::String^ toSysString(const std::string& string)
	{
			return gcnew System::String(string.c_str());
	}
	
	static Color HSLAtoClr(double H, double S, double L, double A)
	{
		double v;
		double r, g, b;
		if (A > 1.0)
		A = 1.0;

		r = L;   // default to gray
		g = L;
		b = L;
		v = (L <= 0.5) ? (L * (1.0 + S)) : (L + S - L * S);
		if (v > 0)
		{
		double m;
		double sv;
		int sextant;
		double fract, vsf, mid1, mid2;

		m = L + L - v;
		sv = (v - m) / v;
		H *= 6.0;
		sextant = (int)H;
		fract = H - sextant;
		vsf = v * sv * fract;
		mid1 = m + vsf;
		mid2 = v - vsf;
		switch (sextant)
		{
			case 0:
			r = v;
			g = mid1;
			b = m;
			break;
			case 1:
			r = mid2;
			g = v;
			b = m;
			break;
			case 2:
			r = m;
			g = v;
			b = mid1;
			break;
			case 3:
			r = m;
			g = mid2;
			b = v;
			break;
			case 4:
			r = mid1;
			g = m;
			b = v;
			break;
			case 5:
			r = v;
			g = m;
			b = mid2;
			break;
		}
		}

		Color rgb = Color::FromArgb(A * 255.0f,r * 255.0f,g * 255.0f,b * 255.0f);
		return rgb;
	}

	Color TranslateColor(Color c1, Color c2, double t) {
		Color c3;
		int R = c1.R+double(double(c2.R-c1.R)*t);
		int G = c1.G+double(double(c2.G-c1.G)*t);
		int B = c1.B+double(double(c2.B-c1.B)*t);
		int A = c1.A+double(double(c2.A-c1.A)*t);
		c3 = Color::FromArgb(R,G,B);
		return c3;
	}

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  ExpressionBox;
	protected: 

	protected: 
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::SplitContainer^  Splitter;
	private: System::Windows::Forms::DataGridView^  TruthTableControl;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TabControl^  KTabs;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  newToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;

	private: System::Windows::Forms::StatusStrip^  statusStrip1;
	private: System::Windows::Forms::ToolStripStatusLabel^  Status;
	private: cliext::vector<System::Windows::Forms::PictureBox^> BulbsVector;
	private: cliext::vector<cliext::vector<System::Windows::Forms::PictureBox^>> SwitchesVector;
	private: cliext::vector<Bitmap^> DiagramsVector;








	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::Panel^  LightBox;
	private: System::Windows::Forms::Button^  ResetTT;
	private: System::Windows::Forms::Button^  ApplyTT;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  homepageToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  copyTruthToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  copyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToFileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveTruthTableToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  countGatesToolStripMenuItem;

















	private: System::ComponentModel::IContainer^  components;






	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->ExpressionBox = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->Splitter = (gcnew System::Windows::Forms::SplitContainer());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->TruthTableControl = (gcnew System::Windows::Forms::DataGridView());
			this->LightBox = (gcnew System::Windows::Forms::Panel());
			this->ResetTT = (gcnew System::Windows::Forms::Button());
			this->ApplyTT = (gcnew System::Windows::Forms::Button());
			this->KTabs = (gcnew System::Windows::Forms::TabControl());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->newToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->copyTruthToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->copyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->countGatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveTruthTableToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->homepageToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->Status = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Splitter))->BeginInit();
			this->Splitter->Panel1->SuspendLayout();
			this->Splitter->Panel2->SuspendLayout();
			this->Splitter->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->TruthTableControl))->BeginInit();
			this->LightBox->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// ExpressionBox
			// 
			this->ExpressionBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->ExpressionBox->Font = (gcnew System::Drawing::Font(L"Consolas", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->ExpressionBox->FormattingEnabled = true;
			this->ExpressionBox->IntegralHeight = false;
			this->ExpressionBox->Location = System::Drawing::Point(86, 33);
			this->ExpressionBox->Name = L"ExpressionBox";
			this->ExpressionBox->Size = System::Drawing::Size(756, 22);
			this->ExpressionBox->TabIndex = 0;
			this->ExpressionBox->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::ExpressionBox_SelectedIndexChanged);
			this->ExpressionBox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::ExpressionBox_KeyDown);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(11, 36);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(70, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Expression = ";
			this->label1->Click += gcnew System::EventHandler(this, &Form1::label1_Click);
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(852, 32);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Generate";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// Splitter
			// 
			this->Splitter->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->Splitter->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->Splitter->Location = System::Drawing::Point(12, 60);
			this->Splitter->Name = L"Splitter";
			// 
			// Splitter.Panel1
			// 
			this->Splitter->Panel1->BackColor = System::Drawing::SystemColors::Control;
			this->Splitter->Panel1->Controls->Add(this->label2);
			this->Splitter->Panel1->Controls->Add(this->TruthTableControl);
			// 
			// Splitter.Panel2
			// 
			this->Splitter->Panel2->BackColor = System::Drawing::SystemColors::Control;
			this->Splitter->Panel2->Controls->Add(this->LightBox);
			this->Splitter->Panel2->Controls->Add(this->KTabs);
			this->Splitter->Panel2->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::splitContainer1_Panel2_Paint);
			this->Splitter->Size = System::Drawing::Size(915, 569);
			this->Splitter->SplitterDistance = 272;
			this->Splitter->TabIndex = 3;
			this->Splitter->SplitterMoving += gcnew System::Windows::Forms::SplitterCancelEventHandler(this, &Form1::Splitter_SplitterMoving);
			this->Splitter->SplitterMoved += gcnew System::Windows::Forms::SplitterEventHandler(this, &Form1::Splitter_SplitterMoved);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 7);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(65, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Truth-Table:";
			// 
			// TruthTableControl
			// 
			this->TruthTableControl->AllowUserToAddRows = false;
			this->TruthTableControl->AllowUserToDeleteRows = false;
			this->TruthTableControl->AllowUserToResizeColumns = false;
			this->TruthTableControl->AllowUserToResizeRows = false;
			this->TruthTableControl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->TruthTableControl->BackgroundColor = System::Drawing::SystemColors::Window;
			this->TruthTableControl->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->TruthTableControl->Location = System::Drawing::Point(2, 26);
			this->TruthTableControl->Name = L"TruthTableControl";
			this->TruthTableControl->Size = System::Drawing::Size(266, 539);
			this->TruthTableControl->TabIndex = 0;
			this->TruthTableControl->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Form1::TruthTableControl_CellContentClick);
			this->TruthTableControl->CellEndEdit += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Form1::TruthTableControl_CellEndEdit);
			this->TruthTableControl->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::TruthTableControl_KeyPress);
			this->TruthTableControl->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::TruthTableControl_MouseClick);
			// 
			// LightBox
			// 
			this->LightBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->LightBox->Controls->Add(this->ResetTT);
			this->LightBox->Controls->Add(this->ApplyTT);
			this->LightBox->Location = System::Drawing::Point(0, 0);
			this->LightBox->Name = L"LightBox";
			this->LightBox->Size = System::Drawing::Size(638, 568);
			this->LightBox->TabIndex = 9;
			this->LightBox->Visible = false;
			// 
			// ResetTT
			// 
			this->ResetTT->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->ResetTT->Location = System::Drawing::Point(324, 257);
			this->ResetTT->MaximumSize = System::Drawing::Size(142, 29);
			this->ResetTT->MinimumSize = System::Drawing::Size(142, 29);
			this->ResetTT->Name = L"ResetTT";
			this->ResetTT->Size = System::Drawing::Size(142, 29);
			this->ResetTT->TabIndex = 9;
			this->ResetTT->Text = L"Reset Changes";
			this->ResetTT->UseVisualStyleBackColor = true;
			this->ResetTT->Click += gcnew System::EventHandler(this, &Form1::ResetTT_Click);
			// 
			// ApplyTT
			// 
			this->ApplyTT->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->ApplyTT->Location = System::Drawing::Point(172, 257);
			this->ApplyTT->MaximumSize = System::Drawing::Size(142, 29);
			this->ApplyTT->MinimumSize = System::Drawing::Size(142, 29);
			this->ApplyTT->Name = L"ApplyTT";
			this->ApplyTT->Size = System::Drawing::Size(142, 29);
			this->ApplyTT->TabIndex = 8;
			this->ApplyTT->Text = L"Apply Changes";
			this->ApplyTT->UseVisualStyleBackColor = true;
			this->ApplyTT->Click += gcnew System::EventHandler(this, &Form1::ApplyTT_Click);
			// 
			// KTabs
			// 
			this->KTabs->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->KTabs->Location = System::Drawing::Point(4, 3);
			this->KTabs->Name = L"KTabs";
			this->KTabs->SelectedIndex = 0;
			this->KTabs->Size = System::Drawing::Size(631, 561);
			this->KTabs->TabIndex = 0;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->fileToolStripMenuItem, 
				this->editToolStripMenuItem, this->helpToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(939, 24);
			this->menuStrip1->TabIndex = 4;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->newToolStripMenuItem, 
				this->openToolStripMenuItem, this->saveToolStripMenuItem, this->exitToolStripMenuItem});
			this->fileToolStripMenuItem->ForeColor = System::Drawing::SystemColors::ControlText;
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// newToolStripMenuItem
			// 
			this->newToolStripMenuItem->Name = L"newToolStripMenuItem";
			this->newToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::N));
			this->newToolStripMenuItem->ShowShortcutKeys = false;
			this->newToolStripMenuItem->Size = System::Drawing::Size(96, 22);
			this->newToolStripMenuItem->Text = L"New";
			this->newToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::newToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::O));
			this->openToolStripMenuItem->ShowShortcutKeys = false;
			this->openToolStripMenuItem->Size = System::Drawing::Size(96, 22);
			this->openToolStripMenuItem->Text = L"Open";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::S));
			this->saveToolStripMenuItem->ShowShortcutKeys = false;
			this->saveToolStripMenuItem->Size = System::Drawing::Size(96, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(96, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this->editToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->copyTruthToolStripMenuItem, 
				this->countGatesToolStripMenuItem, this->saveTruthTableToolStripMenuItem});
			this->editToolStripMenuItem->Name = L"editToolStripMenuItem";
			this->editToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->editToolStripMenuItem->Text = L"Edit";
			// 
			// copyTruthToolStripMenuItem
			// 
			this->copyTruthToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->copyToolStripMenuItem, 
				this->saveToFileToolStripMenuItem});
			this->copyTruthToolStripMenuItem->Name = L"copyTruthToolStripMenuItem";
			this->copyTruthToolStripMenuItem->Size = System::Drawing::Size(181, 22);
			this->copyTruthToolStripMenuItem->Text = L"Truth-Table";
			// 
			// copyToolStripMenuItem
			// 
			this->copyToolStripMenuItem->Name = L"copyToolStripMenuItem";
			this->copyToolStripMenuItem->Size = System::Drawing::Size(130, 22);
			this->copyToolStripMenuItem->Text = L"Copy";
			this->copyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::copyToolStripMenuItem_Click);
			// 
			// saveToFileToolStripMenuItem
			// 
			this->saveToFileToolStripMenuItem->Name = L"saveToFileToolStripMenuItem";
			this->saveToFileToolStripMenuItem->Size = System::Drawing::Size(130, 22);
			this->saveToFileToolStripMenuItem->Text = L"Save toFile";
			this->saveToFileToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveToFileToolStripMenuItem_Click);
			// 
			// countGatesToolStripMenuItem
			// 
			this->countGatesToolStripMenuItem->Name = L"countGatesToolStripMenuItem";
			this->countGatesToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::C));
			this->countGatesToolStripMenuItem->Size = System::Drawing::Size(181, 22);
			this->countGatesToolStripMenuItem->Text = L"Count Gates";
			this->countGatesToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::countGatesToolStripMenuItem_Click);
			// 
			// saveTruthTableToolStripMenuItem
			// 
			this->saveTruthTableToolStripMenuItem->Name = L"saveTruthTableToolStripMenuItem";
			this->saveTruthTableToolStripMenuItem->Size = System::Drawing::Size(181, 22);
			this->saveTruthTableToolStripMenuItem->Text = L"Clear History";
			this->saveTruthTableToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveTruthTableToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->aboutToolStripMenuItem, 
				this->homepageToolStripMenuItem});
			this->helpToolStripMenuItem->ForeColor = System::Drawing::SystemColors::ControlText;
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Alt | System::Windows::Forms::Keys::A));
			this->aboutToolStripMenuItem->ShowShortcutKeys = false;
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(133, 22);
			this->aboutToolStripMenuItem->Text = L"About";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutToolStripMenuItem_Click);
			// 
			// homepageToolStripMenuItem
			// 
			this->homepageToolStripMenuItem->Name = L"homepageToolStripMenuItem";
			this->homepageToolStripMenuItem->Size = System::Drawing::Size(133, 22);
			this->homepageToolStripMenuItem->Text = L"Homepage";
			this->homepageToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::homepageToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->BackColor = System::Drawing::SystemColors::ControlLight;
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->Status});
			this->statusStrip1->Location = System::Drawing::Point(0, 640);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(939, 22);
			this->statusStrip1->TabIndex = 5;
			// 
			// Status
			// 
			this->Status->BackColor = System::Drawing::SystemColors::Control;
			this->Status->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Status->Name = L"Status";
			this->Status->Size = System::Drawing::Size(39, 17);
			this->Status->Text = L"Ready";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// Form1
			// 
			this->AccessibleName = L"";
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(939, 662);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->Splitter);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->ExpressionBox);
			this->Controls->Add(this->menuStrip1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"DLD Guide";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->SizeChanged += gcnew System::EventHandler(this, &Form1::Form1_SizeChanged);
			this->Splitter->Panel1->ResumeLayout(false);
			this->Splitter->Panel1->PerformLayout();
			this->Splitter->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Splitter))->EndInit();
			this->Splitter->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->TruthTableControl))->EndInit();
			this->LightBox->ResumeLayout(false);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void splitContainer1_Panel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
			 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			LoadExpressions();
		 }
private: System::Void Form1_SizeChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
		 
private: void LoadExpressions() {
			ExpressionBox->Items->Clear();
			String^ path = System::IO::Path::GetDirectoryName(Reflection::Assembly::GetExecutingAssembly()->Location);
			ifstream fs(toStdString(path) + "\\history.dat");
			string str;
			while(getline(fs,str)) {
				ExpressionBox->Items->Add(toSysString(str));
			}
			fs.close();

			ExpressionBox->Items->Add("Clear History");
		 }
		 
private: void SaveExpression() {
			 string str = toStdString(ExpressionBox->Text);

			 if(str.length()>0) {
				String^ path = System::IO::Path::GetDirectoryName(Reflection::Assembly::GetExecutingAssembly()->Location);

				ifstream ifs(toStdString(path) + "\\history.dat");
				vector<string>v;
				string fstr;
				while(getline(ifs,fstr)) {
					if(fstr.length()>0) {
						if(fstr!=str) {
							v.push_back(fstr);
						}
					}
				}
				ifs.close();


				ofstream ofs(toStdString(path) + "\\history.dat");
				ofs << str << endl;
				int k = v.size();
				for(int i = 0; i < k; i++) {
					ofs << v[i] << endl;
				}
				ofs.close();
			 }
		 }

private: void ClearExpressions() {
			String^ path = System::IO::Path::GetDirectoryName(Reflection::Assembly::GetExecutingAssembly()->Location);
			ofstream ofs(toStdString(path) + "\\history.dat");
			ofs << "" << endl;
			ofs.close();
		 }

private: void appendExpression(string exp) {
			 string str = toStdString(ExpressionBox->Text);
			 if(str[str.length()-1] != ';') {
				 str += ";";
			 }
			 str += exp;
			 ExpressionBox->Text = toSysString(str);

			 generateTruthTable();
			 KTabs->SelectedIndex = KTabs->TabPages->Count-1;
		 }

private: void generateTruthTable() {

			Expression = toStdString(ExpressionBox->Text);
			Equations = d1.splitEx(Expression);

			Status->Text = "Working";
			this->Refresh();

			for (vector<string>::iterator iter = Equations.begin(); iter != Equations.end();) {
				if(*iter == "") {
					iter = Equations.erase(iter);
				} else {
					++iter;
				}
			}

			if(Equations.size()==0) {
				Status->Text = "Exiting! No expressions to work on.";
				return;
			}

			LightBox->Hide();
			ChangedFuncs.clear();
			AND_Count.clear();
			OR_Count.clear();
			NOT_Count.clear();
			XOR_Count.clear();

			KTabs->TabPages->Clear();
			TruthTableControl->Columns->Clear();
			TruthTableControl->Rows->Clear();

			BulbsVector.clear();
			SwitchesVector.clear();
			DiagramsVector.clear();

			Assembly^ assembly = Assembly::GetExecutingAssembly();
			AssemblyName^ assemblyName = assembly->GetName();
			System::Resources::ResourceManager^ rm = gcnew System::Resources::ResourceManager(assemblyName->Name+".Images", assembly);
			Bitmap^ AND_BMP = (Bitmap^)rm->GetObject("AND");
			Bitmap^ OR_BMP = (Bitmap^)rm->GetObject("OR");
			Bitmap^ XOR_BMP = (Bitmap^)rm->GetObject("XOR");
			Bitmap^ NOT_BMP = (Bitmap^)rm->GetObject("NOT");
			Bitmap^ switch0 = (Bitmap^)rm->GetObject("switch0");
			Bitmap^ bulb0 = (Bitmap^)rm->GetObject("bulb0");
			Bitmap^ bulb1 = (Bitmap^)rm->GetObject("bulb1");


			VarsMap = d1.getVarsMap(Expression);
			TruthTable = d1.getTruthTable(VarsMap);
			if(d1.hasVars(Expression) == true) {
				FunctionVals = d1.getFunctVals(TruthTable,VarsMap,Equations);
			}

			map<char,int>::iterator iter;
			for (iter = VarsMap.begin(); iter != VarsMap.end(); ++iter) {
				String^ str = toSysString(string(&iter->first,1));
				DataGridViewTextBoxColumn^ column = gcnew DataGridViewTextBoxColumn();
				column->Width=30;
				column->HeaderText=str;
				column->HeaderCell->Style->Alignment = DataGridViewContentAlignment::MiddleCenter;
				TruthTableControl->Columns->Add(column);
			}

			for(int i = 0 ; i < Equations.size(); i++) {
				if(Equations[i]=="") { continue; }
				String^ str = "F" + (i+1).ToString();
				DataGridViewTextBoxColumn^ column = gcnew DataGridViewTextBoxColumn();
				column->Width=30;
				column->HeaderText=str;
				//column->Frozen=true;
				column->ToolTipText=toSysString(Equations[i]);
				column->HeaderCell->Style->Alignment = DataGridViewContentAlignment::MiddleCenter;
				TruthTableControl->Columns->Add(column);
				ChangedFuncs.push_back(false);
				AND_Count.push_back(0);
				OR_Count.push_back(0);
				NOT_Count.push_back(0);
				XOR_Count.push_back(0);
			}

			if(TruthTable.size()>0) {
			vector<bool> vb = TruthTable[0];
			for(int j = 0 ; j < vb.size() ; j++) {

				DataGridViewRow^ row = gcnew DataGridViewRow();
				row->HeaderCell->Value=j.ToString();
				TruthTableControl->Rows->Add(row);

				int i;
				for(i = 0 ; i < TruthTable.size() ; i++) {
					bool element = TruthTable[i][j];
					if(element==false) {
						TruthTableControl[i,j]->Style->ForeColor=Color::FromArgb(0,128,128,128);
						TruthTableControl[i,j]->Style->SelectionBackColor = TranslateColor(Color::FromName("Highlight"),Color::FromName("ControlLightLight"),0.3);
					} else {
						TruthTableControl[i,j]->Style->SelectionBackColor = Color::FromName("Highlight");
					}
					TruthTableControl[i,j]->Value = (element==true?1:0);
					TruthTableControl[i,j]->ReadOnly = true;
					TruthTableControl[i,j]->Style->Alignment = DataGridViewContentAlignment::MiddleCenter;
				}

				for(int k = 0 ; k < Equations.size(); k++) {
					if(Equations[k]=="") { continue; }
					bool element = FunctionVals[k][j];
					if(element==false) {
						TruthTableControl[k+i,j]->Style->ForeColor=Color::FromArgb(0,128,128,128);
						TruthTableControl[k+i,j]->Style->SelectionBackColor = TranslateColor(Color::FromName("Highlight"),Color::FromName("ControlLightLight"),0.3);
					} else {
						TruthTableControl[k+i,j]->Style->SelectionBackColor = Color::FromName("Highlight");
					}
					TruthTableControl[k+i,j]->Value = (element==true?1:0);
					TruthTableControl[k+i,j]->ReadOnly = true;
					TruthTableControl[k+i,j]->Style->Alignment = DataGridViewContentAlignment::MiddleCenter;
				}
			}
			TruthTableControl->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			}

			for(int i = 0 ; i < Equations.size(); i++) {
				if(Equations[i]=="") { continue; }

				KTabs->TabPages->Add("F" + (i+1));
				Color c = Color::FromName("ControlLightLight");
				KTabs->TabPages[i]->BackColor = c;
				
				// FUNCTION BOX
				System::Windows::Forms::TextBox^ tb = (gcnew System::Windows::Forms::TextBox());
				KTabs->TabPages[i]->Controls->Add(tb);
				tb->Left=10;
				tb->Top=10;
				tb->Width=KTabs->Width-30;
				tb->Anchor = AnchorStyles::Right + AnchorStyles::Top + AnchorStyles::Left;
				tb->Text = toSysString(Equations[i]);
				
				c = Color::FromName("Control");

				// TAB FOR : K-Map | Diagram
				System::Windows::Forms::TabControl^ tc = (gcnew System::Windows::Forms::TabControl());
				KTabs->TabPages[i]->Controls->Add(tc);
				tc->Left=10;
				tc->Top=40;
				tc->Width=KTabs->Width-30;
				tc->Height=KTabs->Height-220;
				tc->Anchor = AnchorStyles::Right + AnchorStyles::Top + AnchorStyles::Left + AnchorStyles::Bottom;
				tc->TabPages->Add("Gates Diagram");
				tc->TabPages->Add("K-Map Minimization");
				tc->TabPages[0]->BackColor = Color::FromName("ControlLightLight");
				tc->TabPages[1]->BackColor = Color::FromName("ControlLightLight");
				tc->TabPages[0]->AutoScroll = true;

				// Diagram Control
				System::Windows::Forms::PictureBox^ diagram = (gcnew System::Windows::Forms::PictureBox());
				tc->TabPages[0]->Controls->Add(diagram);
				diagram->Left=0;
				diagram->Top=0;
				diagram->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this,&Form1::Diagram_MouseClick);
				diagram->BringToFront();
				diagram->Tag = i;
				//diagram->BorderStyle=BorderStyle::FixedSingle;

				string pfn = d1.toPostFix(Equations[i]);
				BinaryTree bt = d1.toBinaryExpressionTree(pfn);

				/* DRAWING START */
				
				int gapY = 30;
				int gapX = 100;

				vector<BinaryNode *> vct;
				bt.getMap(vct);
				int maxK = bt.getNumLeavesJoints();
				int paddingX = (double(maxK)/double(9))+1;
				int maxX = bt.getXMax()+paddingX;
				int maxY = bt.getYMax();

				int picWidth = (maxX-1)*gapX+gapX*2.5;
				int picHeight = max(maxY*gapY+gapY*2,(VarsMap.size()+1)*gapY+10);

				diagram->Width = picWidth;
				diagram->Height = picHeight;

				Bitmap^ bmp = gcnew Bitmap(picWidth, picHeight);
				bmp->MakeTransparent(Color::FromArgb(255,211,211,211));
				Graphics^ g = Graphics::FromImage(bmp);
				//g->Clear(Color::White);
				
				SolidBrush^ myBrush = gcnew SolidBrush(Color::FromName("HighLight"));
				SolidBrush^ myTxtBrush = gcnew SolidBrush(Color::FromName("ControlText"));
				Pen^ myPen = gcnew Pen(myBrush,3);

				System::Drawing::Drawing2D::GraphicsPath^ gPath = (gcnew System::Drawing::Drawing2D::GraphicsPath());
				StringFormat^ strFormat = gcnew StringFormat();
				strFormat->Alignment = StringAlignment::Center;

				g->SmoothingMode = Drawing2D::SmoothingMode::HighQuality;   

				int k = 0;

				int lastLevel = 10;

				for (vector<BinaryNode *>::iterator iter = vct.begin(); iter != vct.end(); ++iter) {
					BinaryNode * node = *(iter);
					BinaryNode * l = node->left;
					BinaryNode * r = node->right;
					vector<lineXY> lines;

					int offset = 10;
					Pen^ cPen;

					if(l!=NULL) {
						if(node->info=="'") {
							offset = 0;
						}
						
						if(node->left->position.x+paddingX == maxX) {
							char ch = node->left->info[0];
							if(VarsMap.count(ch) == 0) { continue; }
							int pos = VarsMap[ch];
							pos++;
							
							k++;
							lastLevel = (double(k)/double(maxK+1))*paddingX*double(gapX);

							lines = d1.toRectLines(lineXY((maxX-node->position.x-1)*gapX-3+gapX, (maxY-node->position.y+1)*gapY-3+gapY+offset, gapX/2+18, gapY*pos+21),lastLevel);
							
							cPen = gcnew Pen(gcnew SolidBrush(HSLAtoClr(double(k-1)/double(maxK),0.5,0.7,1)),3);
						} else {
							lines = d1.toRectLines(lineXY((maxX-node->position.x-1)*gapX-3+gapX, (maxY-node->position.y+1)*gapY-3+gapY+offset, (maxX-l->position.x-1)*gapX-3+gapX, (maxY-l->position.y+1)*gapY-3+gapY));
							cPen = myPen;
						}

						for (vector<lineXY>::iterator iter = lines.begin(); iter != lines.end(); ++iter) {
							g->DrawLine(cPen, iter->x1,iter->y1,iter->x2,iter->y2);
						}
					}
					if(r!=NULL) {
						if(node->right->position.x+paddingX == maxX) {
							char ch = node->right->info[0];
							k++;
							lastLevel = (double(k)/double(maxK+1))*paddingX*double(gapX);
							if(VarsMap.count(ch) == 0) { continue; }

							int pos = VarsMap[ch];
							pos++;
							lines = d1.toRectLines(lineXY((maxX-node->position.x-1)*gapX-3+gapX, (maxY-node->position.y+1)*gapY-3+gapY-offset, gapX/2+18, gapY*pos+21),lastLevel);

							cPen = gcnew Pen(gcnew SolidBrush(HSLAtoClr(double(k-1)/double(maxK),0.5,0.7,1)),3);
						} else {
							lines = d1.toRectLines(lineXY((maxX-node->position.x-1)*gapX-3+gapX, (maxY-node->position.y+1)*gapY-3+gapY-offset, (maxX-r->position.x-1)*gapX-3+gapX, (maxY-r->position.y+1)*gapY-3+gapY));
							cPen = myPen;
						}
						
						for (vector<lineXY>::iterator iter = lines.begin(); iter != lines.end(); ++iter) {
							g->DrawLine(cPen, iter->x1,iter->y1,iter->x2,iter->y2);
						}
					}
				}
				
				//if(d1.hasVars(Equations[i]) == false) {
				if(bt.root == 0) {
					// no operators, no lines
					bt;

				} else if(Equations[i].size()>1) {
					// some func performed
					g->DrawLine(myPen, (maxX-bt.root->position.x-1)*gapX-3+gapX, (maxY-bt.root->position.y+1)*gapY-3+gapY, (maxX-bt.root->position.x-1)*gapX-3+gapX*2, (maxY-bt.root->position.y+1)*gapY-3+gapY);
				} else {
					// 1 var; 1 line

					if(VarsMap.count(Equations[i][0]) == 0) {
					} else {
						int idx = VarsMap[Equations[i][0]]+1;
						vector<lineXY> lines;
						lines = d1.toRectLines(lineXY((maxX-bt.root->position.x-1)*gapX-3+gapX*2, (maxY-bt.root->position.y+1)*gapY-3+gapY, gapX/2+18, gapY*idx+21));
						
						Pen^ cPen = myPen;
						for (vector<lineXY>::iterator iter = lines.begin(); iter != lines.end(); ++iter) {
							g->DrawLine(cPen, iter->x1,iter->y1,iter->x2,iter->y2);
						}
					}
				}

				System::Windows::Forms::PictureBox^ bulb = (gcnew System::Windows::Forms::PictureBox());
				diagram->Controls->Add(bulb);
				bulb->Left= (maxX-bt.root->position.x-1)*gapX-3+gapX*2-27;
				bulb->Top = (maxY-bt.root->position.y+1)*gapY-3+gapY-15;
				bulb->Width=35;
				bulb->Height=30;
				bulb->BringToFront();
				bulb->BackColor = Color::Transparent;
				BulbsVector.push_back(bulb);
				
				if(FunctionVals.size()>0) {
					if(FunctionVals[i][0]==true) {
						bulb->Image = bulb1;
					} else {
						bulb->Image = bulb0;
					}
				}

				int n = 1;
				cliext::vector<System::Windows::Forms::PictureBox^> svct;

				for (map<char,int>::iterator iter = VarsMap.begin(); iter != VarsMap.end(); ++iter) {
					PointF pnf(gapX/2, gapY*n);
					gPath->AddString(toSysString(string(&iter->first,1)), gcnew FontFamily("Segoe UI"), (int)FontStyle::Regular, 26, pnf, strFormat);
					n++;

						System::Windows::Forms::PictureBox^ swich = (gcnew System::Windows::Forms::PictureBox());
						diagram->Controls->Add(swich);
						swich->Left= gapX/2-47;
						swich->Top= gapY*n-26;
						swich->Width=35;
						swich->Height=30;
						swich->BringToFront();
						swich->BackColor = Color::Transparent;
						swich->Tag = System::Drawing::Rectangle(i,n-1,0,0);
						swich->Image = switch0;
						swich->Click += gcnew System::EventHandler(this,&Form1::toggleSwitch);
						svct.push_back(swich);
				}

				SwitchesVector.push_back(svct);

				gPath->AddString("F" + (i+1).ToString(), gcnew FontFamily("Segoe UI"), (int)FontStyle::Regular, 26, PointF((maxX-bt.root->position.x-1)*gapX-3+gapX*1.5, (maxY-bt.root->position.y+1)*gapY-8), strFormat);

				g->FillPath(myTxtBrush, gPath);

				for (vector<BinaryNode *>::iterator iter = vct.begin(); iter != vct.end(); ++iter) {
					BinaryNode * node = *(iter);
					Bitmap ^ bmp;
					string t;
					if(node->info == "*") {
						bmp = AND_BMP;
						AND_Count[i]++;
						t = "operator";
					} else if(node->info == "+") {
						bmp = OR_BMP;
						OR_Count[i]++;
						t = "operator";
					} else if(node->info == "^") {
						bmp = XOR_BMP;
						XOR_Count[i]++;
						t = "operator";
					} else if(node->info == "'") {
						bmp = NOT_BMP;
						NOT_Count[i]++;
						t = "operator";
					} else {
						t = "operand";
					}

					if(t == "operator") {
						g->DrawImage(bmp, (maxX-node->position.x-1)*gapX-3+gapX-50, (maxY-node->position.y+1)*gapY-3+gapY-50, 100, 100);
						string expression = bt.getExpression(node);

						System::Windows::Forms::PictureBox^ helper = (gcnew System::Windows::Forms::PictureBox());
						diagram->Controls->Add(helper);
						helper->Left=(maxX-node->position.x-1)*gapX-3+gapX*0.65;
						helper->Top=(maxY-node->position.y+1)*gapY-3+gapY-gapY;
						helper->Width=70;
						helper->Height=60;
						helper->BringToFront();
						helper->BackColor = Color::Transparent;
						helper->Tag = toSysString(expression);
						helper->DoubleClick += gcnew System::EventHandler(this,&Form1::gateDoubleClick);
						helper->MouseHover += gcnew System::EventHandler(this,&Form1::gateHover);

					}
					
				}

				DiagramsVector.push_back(bmp);
				diagram->Image = bmp;

				/* DRAWING END */

				string kAns;
				// Try making K-Map
				try {

					if(FunctionVals.size()==0) {
						throw("N/A");
					}

					kMap k1 = d1.generateKMap(TruthTable, VarsMap, FunctionVals[i]);
					d1.solveKMap(k1);
				
					// KMap-Control
					System::Windows::Forms::DataGridView^ dg = (gcnew System::Windows::Forms::DataGridView());
					tc->TabPages[1]->Controls->Add(dg);
					dg->Left=0;
					dg->Top=0;
					dg->Tag=i;
					dg->Width=tc->ClientSize.Width-9;
					dg->Height=tc->ClientSize.Height-26;
					dg->Anchor = AnchorStyles::Right + AnchorStyles::Top + AnchorStyles::Left + AnchorStyles::Bottom;
					dg->BackgroundColor = c;
					dg->BringToFront();
					dg->AllowUserToAddRows = false;
					dg->AllowUserToDeleteRows = false;
					dg->AllowUserToOrderColumns = true;
					dg->AllowUserToResizeColumns = false;
					dg->AllowUserToResizeRows = false;
					dg->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this,&Form1::KMap_MouseClick);

					for(int i = 0 ; i < k1.kmap.size(); i++) {
						DataGridViewTextBoxColumn^ column = gcnew DataGridViewTextBoxColumn();
						column->Width=30;
						column->HeaderText=toSysString(k1.gW[i]);
						column->HeaderCell->Style->Alignment = DataGridViewContentAlignment::MiddleCenter;
						dg->Columns->Add(column);
					}
				
					vector<bool> vb = k1.kmap[0];
					for(int j = 0 ; j < vb.size() ; j++) {

						DataGridViewRow^ row = gcnew DataGridViewRow();
						row->HeaderCell->Value=toSysString(k1.gH[j]);
						dg->Rows->Add(row);
					
						int i;
						for(i = 0 ; i < k1.kmap.size() ; i++) {
							bool element = k1.kmap[i][j];
							dg[i,j]->Value = (element==true?1:0);
							if(element==false) {
								dg[i,j]->Style->ForeColor=Color::FromArgb(0,128,128,128);
							} else {
								dg[i,j]->Style->ForeColor=Color::FromName("ControlText");
							}
							dg[i,j]->ReadOnly = true;
							dg[i,j]->Style->Alignment = DataGridViewContentAlignment::MiddleCenter;
							dg[i,j]->Style->BackColor = Color::FromName("ControlLightLight");
							dg[i,j]->Style->SelectionBackColor = Color::FromName("ControlLightLight");
							dg[i,j]->Style->SelectionForeColor = dg[i,j]->Style->ForeColor;
						}
					
					}
					dg->RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;

					for(int i = 0 ; i < k1.EPIs.size() ; i++) {
						Color crgb = HSLAtoClr(double(i)/double(k1.EPIs.size()),0.6,0.75,1);
						vector<pointXY> ps = k1.EPIs[i];
						for(int j = 0 ; j < ps.size() ; j++) {
							Color ccrgb = TranslateColor(dg[ps[j].x,ps[j].y]->Style->BackColor,crgb,0.33);
							Color scrgb = TranslateColor(Color::FromName("ControlLightLight"),ccrgb,0.65);
							dg[ps[j].x,ps[j].y]->Style->BackColor = ccrgb;
							dg[ps[j].x,ps[j].y]->Style->SelectionBackColor = scrgb;
						}
					}

					if(k1.res == FunctionVals[i]) {
						kAns = k1.EPITerms;
					} else {
						//kAns = "Not Available";
						int numTrue = 0;
						int numFalse= 0;

						for (vector<bool>::iterator iter = FunctionVals[i].begin(); iter != FunctionVals[i].end(); ++iter) {
							if(*iter == true) {
								numTrue++;
							} else {
								numFalse++;
							}
						}

						if(numTrue>= numFalse) {
							kAns = d1.toPOSx(TruthTable, VarsMap, FunctionVals[i]);
						} else {
							kAns = d1.toSOPx(TruthTable, VarsMap, FunctionVals[i]);
						}
					}

				} catch(char * e) {
					kAns = "Not Available";
				}

				System::Drawing::Font^ CodeFont = (gcnew System::Drawing::Font(L"Consolas", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));

				System::Windows::Forms::Button^ plus;

				// MINIMIZED FORM: LABEL & TXT & +
				System::Windows::Forms::Label^ lb = (gcnew System::Windows::Forms::Label());
				KTabs->TabPages[i]->Controls->Add(lb);
				lb->Left=10;
				lb->Top=KTabs->Height-170;
				lb->Anchor = AnchorStyles::Left + AnchorStyles::Bottom;
				lb->Text = "Minimized: ";

				tb = (gcnew System::Windows::Forms::TextBox());
				KTabs->TabPages[i]->Controls->Add(tb);
				tb->Left=80;
				tb->Top=KTabs->Height-172;
				tb->Width=KTabs->Width-135;
				tb->Height=23;
				tb->Anchor = AnchorStyles::Right + AnchorStyles::Bottom + AnchorStyles::Left;
				tb->Text = toSysString(kAns);
				tb->Font = CodeFont;
				tb->BringToFront();

				plus = (gcnew System::Windows::Forms::Button());
				KTabs->TabPages[i]->Controls->Add(plus);
				plus->Left=KTabs->Width-52;
				plus->Top=KTabs->Height-173;
				plus->Width=30;
				plus->Height=24;
				plus->Anchor = AnchorStyles::Right + AnchorStyles::Bottom;
				plus->Text = "+";
				plus->Tag = toSysString(kAns);
				plus->Font = CodeFont;
				plus->BackColor = Color::Transparent;
				plus->Click += gcnew System::EventHandler(this,&Form1::Plus_Click);
				
				// SOP FORM: LABEL & TXT & +
				lb = (gcnew System::Windows::Forms::Label());
				KTabs->TabPages[i]->Controls->Add(lb);
				lb->Left=10;
				lb->Top=KTabs->Height-140;
				lb->Anchor = AnchorStyles::Left + AnchorStyles::Bottom;
				lb->Text = "SOP: ";

				String^ SOP_str;
				
				if(FunctionVals.size()>0) {
					SOP_str = toSysString(d1.toSOP(FunctionVals[i]));
				}

				tb = (gcnew System::Windows::Forms::TextBox());
				KTabs->TabPages[i]->Controls->Add(tb);
				tb->Left=80;
				tb->Top=KTabs->Height-142;
				tb->Width=KTabs->Width-135;
				tb->Height=23;
				tb->Anchor = AnchorStyles::Right + AnchorStyles::Bottom + AnchorStyles::Left;
				tb->Text = SOP_str;
				tb->Font = CodeFont;
				tb->BringToFront();

				plus = (gcnew System::Windows::Forms::Button());
				KTabs->TabPages[i]->Controls->Add(plus);
				plus->Left=KTabs->Width-52;
				plus->Top=KTabs->Height-143;
				plus->Width=30;
				plus->Height=24;
				plus->Anchor = AnchorStyles::Right + AnchorStyles::Bottom;
				plus->Text = "+";
				plus->Tag = SOP_str;
				plus->Font = CodeFont;
				plus->BackColor = Color::Transparent;
				plus->Enabled = false;
				
				// SOP(F) FORM: LABEL & TXT & +
				lb = (gcnew System::Windows::Forms::Label());
				KTabs->TabPages[i]->Controls->Add(lb);
				lb->Left=10;
				lb->Top=KTabs->Height-110;
				lb->Anchor = AnchorStyles::Left + AnchorStyles::Bottom;
				lb->Text = "SOP(F): ";

				String^ SOPF_str;
				
				if(FunctionVals.size()>0) {
					SOPF_str = toSysString(d1.toSOPx(TruthTable,VarsMap,FunctionVals[i]));
				}

				tb = (gcnew System::Windows::Forms::TextBox());
				KTabs->TabPages[i]->Controls->Add(tb);
				tb->Left=80;
				tb->Top=KTabs->Height-112;
				tb->Width=KTabs->Width-135;
				tb->Height=23;
				tb->Anchor = AnchorStyles::Right + AnchorStyles::Bottom + AnchorStyles::Left;
				tb->Text = SOPF_str;
				tb->Font = CodeFont;
				tb->BringToFront();

				plus = (gcnew System::Windows::Forms::Button());
				KTabs->TabPages[i]->Controls->Add(plus);
				plus->Left=KTabs->Width-52;
				plus->Top=KTabs->Height-113;
				plus->Width=30;
				plus->Height=24;
				plus->Anchor = AnchorStyles::Right + AnchorStyles::Bottom;
				plus->Text = "+";
				plus->Tag = SOPF_str;
				plus->Font = CodeFont;
				plus->BackColor = Color::Transparent;
				plus->Click += gcnew System::EventHandler(this,&Form1::Plus_Click);
				
				// POS FORM: LABEL & TXT & +
				lb = (gcnew System::Windows::Forms::Label());
				KTabs->TabPages[i]->Controls->Add(lb);
				lb->Left=10;
				lb->Top=KTabs->Height-80;
				lb->Anchor = AnchorStyles::Left + AnchorStyles::Bottom;
				lb->Text = "POS: ";

				String^ POS_str;
				
				if(FunctionVals.size()>0) {
					POS_str = toSysString(d1.toPOS(FunctionVals[i]));
				}

				tb = (gcnew System::Windows::Forms::TextBox());
				KTabs->TabPages[i]->Controls->Add(tb);
				tb->Left=80;
				tb->Top=KTabs->Height-82;
				tb->Width=KTabs->Width-135;
				tb->Height=23;
				tb->Anchor = AnchorStyles::Right + AnchorStyles::Bottom + AnchorStyles::Left;
				tb->Text = POS_str;
				tb->Font = CodeFont;
				tb->BringToFront();

				plus = (gcnew System::Windows::Forms::Button());
				KTabs->TabPages[i]->Controls->Add(plus);
				plus->Left=KTabs->Width-52;
				plus->Top=KTabs->Height-83;
				plus->Width=30;
				plus->Height=24;
				plus->Anchor = AnchorStyles::Right + AnchorStyles::Bottom;
				plus->Text = "+";
				plus->Tag = POS_str;
				plus->Font = CodeFont;
				plus->BackColor = Color::Transparent;
				plus->Enabled = false;
				
				// POS(F): LABEL & TXT & +
				lb = (gcnew System::Windows::Forms::Label());
				KTabs->TabPages[i]->Controls->Add(lb);
				lb->Left=10;
				lb->Top=KTabs->Height-50;
				lb->Anchor = AnchorStyles::Left + AnchorStyles::Bottom;
				lb->Text = "POS(F): ";
				lb->BringToFront();

				String^ POSF_str;
				
				if(FunctionVals.size()>0) {
					POSF_str = toSysString(d1.toPOSx(TruthTable,VarsMap,FunctionVals[i]));
				}

				tb = (gcnew System::Windows::Forms::TextBox());
				KTabs->TabPages[i]->Controls->Add(tb);
				tb->Left=80;
				tb->Top=KTabs->Height-52;
				tb->Width=KTabs->Width-135;
				tb->Height=23;
				tb->Anchor = AnchorStyles::Right + AnchorStyles::Bottom + AnchorStyles::Left;
				tb->Text = POSF_str;
				tb->Font = CodeFont;
				tb->BringToFront();

				plus = (gcnew System::Windows::Forms::Button());
				KTabs->TabPages[i]->Controls->Add(plus);
				plus->Left=KTabs->Width-52;
				plus->Top=KTabs->Height-53;
				plus->Width=30;
				plus->Height=24;
				plus->Anchor = AnchorStyles::Right + AnchorStyles::Bottom;
				plus->Text = "+";
				plus->Tag = POSF_str;
				plus->Font = CodeFont;
				plus->BackColor = Color::Transparent;
				plus->Click += gcnew System::EventHandler(this,&Form1::Plus_Click);

			}

			Status->Text = "Ready";

		 }

private: System::Void Splitter_SplitterMoved(System::Object^  sender, System::Windows::Forms::SplitterEventArgs^  e) {
		 }
private: System::Void Splitter_SplitterMoving(System::Object^  sender, System::Windows::Forms::SplitterCancelEventArgs^  e) {
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 SaveExpression();
			 LoadExpressions();
			 generateTruthTable();
		 }

private: System::Void ExpressionBox_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
			 if(e->KeyValue==13) {
				 e->SuppressKeyPress = true;
				 SaveExpression();
				 LoadExpressions();
				 generateTruthTable();
			 }
		 }
private: System::Void ExpressionBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(ExpressionBox->SelectedIndex==ExpressionBox->Items->Count-1) {
				 ClearExpressions();
				 ExpressionBox->SelectedText="";
				 ExpressionBox->Items->Clear();
			 }
		 }
		 
private: System::Void gateDoubleClick(System::Object^  sender, System::EventArgs^  e) {
			 System::Windows::Forms::PictureBox^ pb = (System::Windows::Forms::PictureBox^) sender;
			 appendExpression(toStdString((System::String^)pb->Tag));
		 }
		 
private: System::Void gateHover(System::Object^  sender, System::EventArgs^  e) {
			System::Windows::Forms::PictureBox^ pb = (System::Windows::Forms::PictureBox^) sender;

			ToolTip^ tt = gcnew ToolTip();
			tt->SetToolTip(pb, (System::String^)pb->Tag);

		 }

private: System::Void toggleSwitch(System::Object^  sender, System::EventArgs^  e) {
			System::Windows::Forms::PictureBox^ pb = (System::Windows::Forms::PictureBox^) sender;
			System::Drawing::Rectangle^ rect;
			rect = ((System::Drawing::Rectangle^)pb->Tag);

			Assembly^ assembly = Assembly::GetExecutingAssembly();
			AssemblyName^ assemblyName = assembly->GetName();
			System::Resources::ResourceManager^ rm = gcnew System::Resources::ResourceManager(assemblyName->Name+".Images", assembly);
			Bitmap^ switch0 = (Bitmap^)rm->GetObject("switch0");
			Bitmap^ switch1 = (Bitmap^)rm->GetObject("switch1");
			Bitmap^ bulb0 = (Bitmap^)rm->GetObject("bulb0");
			Bitmap^ bulb1 = (Bitmap^)rm->GetObject("bulb1");

			int i = rect->X;
			int n = rect->Y;
			bool s = rect->Width;
			System::Windows::Forms::PictureBox^ bulb = BulbsVector[i];

			if(s==true) {
				pb->Image = switch0;
				rect->Width = 0;
			} else {
				pb->Image = switch1;
				rect->Width = 1;
			}

			vector<bool> states;

			for(int j = 0; j < SwitchesVector[i].size(); j++) {
				System::Windows::Forms::PictureBox^ p = SwitchesVector[i][j];
				System::Drawing::Rectangle^ r = (System::Drawing::Rectangle^)(p->Tag);
				states.push_back((bool)(r->Width));
			}
			
			int maxJ = TruthTable[0].size();
			bool result = false;
			for(int j = 0; j < maxJ; j++) {
				bool matchedStates = true;
				for(int k = 0; k < TruthTable.size(); k++) {
					if(TruthTable[k][j] != states[k]) {
						matchedStates = false;
					}
				}
				if(matchedStates == true) {
					result = FunctionVals[i][j];
					break;
				}
			}

			if(result==true) {
				bulb->Image = bulb1;
			} else {
				bulb->Image = bulb0;
			}

		 }

private: System::Void Plus_Click(System::Object^ sender, System::EventArgs^  e) {
			System::Windows::Forms::Button^ btn = (System::Windows::Forms::Button^) sender;
			String^ eq = (String^)btn->Tag;

			String^ cur = ExpressionBox->Text;

			if(cur[cur->Length-1] == ';') {
				ExpressionBox->Text += eq;
			} else {
				ExpressionBox->Text += ";" + eq;
			}

			generateTruthTable();

		 }

private: System::Void Diagram_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

			if (e->Button != System::Windows::Forms::MouseButtons::Right) {
				return;
			}

			System::Windows::Forms::ContextMenuStrip ^ context = gcnew System::Windows::Forms::ContextMenuStrip;
			
			ToolStripMenuItem ^ copy_menu = gcnew ToolStripMenuItem(L"Copy");
			copy_menu->Click += gcnew System::EventHandler(this,&Form1::DiagramCopy);
			context->Items->Add(copy_menu);

			ToolStripMenuItem ^ save_menu = gcnew ToolStripMenuItem(L"Save to File");
			save_menu->Click += gcnew System::EventHandler(this,&Form1::DiagramSave);
			context->Items->Add(save_menu);

			context->Items->Add(gcnew ToolStripSeparator());
			context->Show((System::Windows::Forms::Control^) sender, e->Location);
		 }

		 void DiagramCopy(System::Object^ sender, System::EventArgs^  e) {
			System::Windows::Forms::ToolStripMenuItem^ menuItem = (System::Windows::Forms::ToolStripMenuItem^) sender;
			System::Windows::Forms::ContextMenuStrip^ menu = (System::Windows::Forms::ContextMenuStrip^) menuItem->Owner;
			Control^ sourceControl = menu->SourceControl;
			System::Windows::Forms::PictureBox^ dg = (System::Windows::Forms::PictureBox^) sourceControl;

			Bitmap^ bmp = DiagramsVector[(int) dg->Tag];
			Bitmap^ newBmp = gcnew Bitmap(bmp->Width, bmp->Height);

			Graphics^ g = Graphics::FromImage(newBmp);
			g->Clear(Color::White);
			g->DrawImage(bmp, 0, 0, bmp->Width, bmp->Height);

			/*
			IO::MemoryStream^ ms = gcnew IO::MemoryStream();
			bmp->Save(ms, System::Drawing::Imaging::ImageFormat::Png);
			DataObject^ m_data = gcnew DataObject();
			m_data->SetData("PNG", true, ms);
			Clipboard::SetDataObject(m_data, true);
			*/

			Clipboard::SetImage(newBmp);

		 }

		 void DiagramSave(System::Object^ sender, System::EventArgs^  e) {
			System::Windows::Forms::ToolStripMenuItem^ menuItem = (System::Windows::Forms::ToolStripMenuItem^) sender;
			System::Windows::Forms::ContextMenuStrip^ menu = (System::Windows::Forms::ContextMenuStrip^) menuItem->Owner;
			Control^ sourceControl = menu->SourceControl;
			System::Windows::Forms::PictureBox^ dg = (System::Windows::Forms::PictureBox^) sourceControl;

			Bitmap^ bmp = DiagramsVector[(int) dg->Tag];
			
			saveFileDialog1->Filter="*HTML files (*.png)|*.png|All files (*.*)|*.*";
			saveFileDialog1->DefaultExt=".png";
			saveFileDialog1->Title="Save Diagram";
			saveFileDialog1->FileName="Diagram" + (int) dg->Tag + ".png";
			System::Windows::Forms::DialogResult^ result = saveFileDialog1->ShowDialog();
			String^restr = result->ToString();
			if (restr == "OK") {
				bmp->Save(saveFileDialog1->FileName, System::Drawing::Imaging::ImageFormat::Png);
			}

		 }


private: System::Void KMap_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if (e->Button != System::Windows::Forms::MouseButtons::Right) {
				return;
			}

			System::Windows::Forms::ContextMenuStrip ^ context = gcnew System::Windows::Forms::ContextMenuStrip;
			
			ToolStripMenuItem ^ copy_menu = gcnew ToolStripMenuItem(L"Copy");
			copy_menu->Click += gcnew System::EventHandler(this,&Form1::KMapCopy);
			context->Items->Add(copy_menu);

			ToolStripMenuItem ^ save_menu = gcnew ToolStripMenuItem(L"Save to File");
			save_menu->Click += gcnew System::EventHandler(this,&Form1::KMapSave);
			context->Items->Add(save_menu);

			context->Items->Add(gcnew ToolStripSeparator());
			context->Show((System::Windows::Forms::Control^) sender, e->Location);

		 }

		 void KMapCopy(System::Object^ sender, System::EventArgs^  e) {
			System::Windows::Forms::ToolStripMenuItem^ menuItem = (System::Windows::Forms::ToolStripMenuItem^) sender;
			System::Windows::Forms::ContextMenuStrip^ menu = (System::Windows::Forms::ContextMenuStrip^) menuItem->Owner;
			Control^ sourceControl = menu->SourceControl;

			 setHTMLClipboard(generateKMapHTML((DataGridView^) sourceControl));
		 }

		 void KMapSave(System::Object^ sender, System::EventArgs^  e) {
			System::Windows::Forms::ToolStripMenuItem^ menuItem = (System::Windows::Forms::ToolStripMenuItem^) sender;
			System::Windows::Forms::ContextMenuStrip^ menu = (System::Windows::Forms::ContextMenuStrip^) menuItem->Owner;
			Control^ sourceControl = menu->SourceControl;
			DataGridView^ dgv = (DataGridView^) sourceControl;

			string str;
			str = "<div style='border-collapse:collapse;font-family:\"Segoe UI\"'>F = " + Equations[(int) dgv->Tag] + "<br/></div>" + generateKMapHTML(dgv);
			
			saveFileDialog1->Filter="*HTML files (*.html)|*.html|All files (*.*)|*.*";
			saveFileDialog1->DefaultExt=".html";
			saveFileDialog1->Title="Save K-Map";
			saveFileDialog1->FileName="KMap" + (int) dgv->Tag + ".html";
			System::Windows::Forms::DialogResult^ result = saveFileDialog1->ShowDialog();
			String^restr = result->ToString();
			if (restr == "OK") {
				ofstream fstr(toStdString(saveFileDialog1->FileName));
				fstr << str;
				fstr.close();
			}

		 }

		 string generateKMapHTML(DataGridView^ kMap) {
			 string html = "<table style='border-collapse:collapse;font-family:\"Segoe UI\"'>";
			 string in_html = "";

			 System::Windows::Forms::DataGridViewRowCollection^ rows = kMap->Rows;

			 for(int i = 0; i < rows->Count; i++) {
				 System::Windows::Forms::DataGridViewCellCollection^ cells = rows[i]->Cells;
				 in_html += "<tr>";

				 in_html += "<td style='border:1px solid rgb(120,120,140); width:35px; text-align:center;'>";
				 in_html += toStdString(kMap->Rows[i]->HeaderCell->Value->ToString());
				 in_html += "</td>";

				 for(int j = 0; j < cells->Count; j++) {
					 System::Windows::Forms::DataGridViewCell^ cell = cells[j];
						 in_html += "<td style='border:1px solid rgb(140,140,160); background-color:rgb(" + toStdString(cell->Style->BackColor.R.ToString()) + "," + toStdString(cell->Style->BackColor.G.ToString()) + "," + toStdString(cell->Style->BackColor.B.ToString()) + "); width:35px; text-align:center;'>";
						 if(cell->Value->ToString()=="0") {
							 in_html += "0";
						 } else {
							 in_html += "1";
						 }
						 in_html += "</td>";
				 }
				in_html += "</tr>";
			 }

			 html += "<tr>";

			 html += "<td style='border:1px solid rgb(120,120,140); width:35px'>";
			 html += "</td>";

			 for(int i = 0; i < kMap->Columns->Count; i++) {
				html += "<td style='border:1px solid rgb(140,140,160); width:35px; text-align:center;'>";
				html += toStdString(kMap->Columns[i]->HeaderText);
				html += "</td>";
			 }
			 html += "</tr>";

			 html += in_html + "</table>";
			
			 return html;
		 }


private: System::Void TruthTableControl_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

			if (e->Button != System::Windows::Forms::MouseButtons::Right) {
				return;
			}
			
			System::Windows::Forms::ContextMenuStrip ^ context = gcnew System::Windows::Forms::ContextMenuStrip;
			
			ToolStripMenuItem ^ copy_menu = gcnew ToolStripMenuItem(L"Copy");
			copy_menu->Click += gcnew System::EventHandler(this,&Form1::TTCopy);
			context->Items->Add(copy_menu);

			ToolStripMenuItem ^ save_menu = gcnew ToolStripMenuItem(L"Save to File");
			save_menu->Click += gcnew System::EventHandler(this,&Form1::TTSave);
			context->Items->Add(save_menu);

			context->Items->Add(gcnew ToolStripSeparator());
			
			ToolStripMenuItem ^ select_all_menu = gcnew ToolStripMenuItem(L"Select All");
			select_all_menu->Click += gcnew System::EventHandler(this,&Form1::TTSelectAll);
			context->Items->Add(select_all_menu);

			ToolStripMenuItem ^ select_vars_menu = gcnew ToolStripMenuItem(L"Select Variables");
			select_vars_menu->Click += gcnew System::EventHandler(this,&Form1::TTSelectVars);
			context->Items->Add(select_vars_menu);

			ToolStripMenuItem ^ select_funcs_menu = gcnew ToolStripMenuItem(L"Select Functions");
			select_funcs_menu->Click += gcnew System::EventHandler(this,&Form1::TTSelectFuncs);
			context->Items->Add(select_funcs_menu);

			ToolStripMenuItem ^ invert_all_menu = gcnew ToolStripMenuItem(L"Invert Selection");
			invert_all_menu->Click += gcnew System::EventHandler(this,&Form1::TTInvert);
			context->Items->Add(invert_all_menu);

			context->Show((System::Windows::Forms::Control^) sender, e->Location);

		 }
		 
		 void TTCopy(System::Object^ sender, System::EventArgs^  e) {
			 setHTMLClipboard(generateTruthTableHTML(false));
		 }

		 void TTSave(System::Object^ sender, System::EventArgs^  e) {
			string html = generateTruthTableHTML(true);

			string str = "<div style='border-collapse:collapse;font-family:\"Segoe UI\"'>";
			for(int i = 0; i < Equations.size(); i++) {
				str += "F" + toStdString((i+1).ToString()) + " = " + Equations[i] + "<br/>";
			}
			str += "</div>";

			html = str + html;

			saveFileDialog1->Filter="*HTML files (*.html)|*.html|All files (*.*)|*.*";
			saveFileDialog1->DefaultExt=".html";
			saveFileDialog1->Title="Save Truth-Table";
			saveFileDialog1->FileName="TruthTable.html";
			System::Windows::Forms::DialogResult^ result = saveFileDialog1->ShowDialog();
			String^restr = result->ToString();
			if (restr == "OK") {
				ofstream fstr(toStdString(saveFileDialog1->FileName));
				fstr << html;
				fstr.close();
			}
		 }

		 string generateTruthTableHTML(bool selectAll) {
			 string html = "<table style='border-collapse:collapse;font-family:\"Segoe UI\"'>";
			 string in_html = "";
			 vector<bool> colsIn;

			 System::Windows::Forms::DataGridViewRowCollection^ rows = TruthTableControl->Rows;
			 for(int i = 0; i < rows[0]->Cells->Count; i++) {
				 colsIn.push_back(false);
			 }

			 for(int i = 0; i < rows->Count; i++) {
				 bool found = false;
				 System::Windows::Forms::DataGridViewCellCollection^ cells = rows[i]->Cells;

				 for(int j = 0; j < cells->Count; j++) {
					 System::Windows::Forms::DataGridViewCell^ cell = cells[j];
					 if(cell->Selected==true || selectAll == true) {
						 colsIn[j] = true;
						 if(found==false) {
							 in_html += "<tr>";
							 found = true;
						 }
						 in_html += "<td style='border:1px solid rgb(180,180,200); width:25px; text-align:center;'>";
						 if(cell->Value->ToString()=="0") {
							 in_html += "0";
						 } else {
							 in_html += "1";
						 }
						 in_html += "</td>";
					 }
				 }
				 if(found==true) {
					in_html += "</tr>";
				 }
			 }

			 html += "<tr>";
			 for(int i = 0; i < TruthTableControl->Columns->Count; i++) {
				 if(colsIn[i] == true) {
					html += "<td style='border:1px solid rgb(140,140,160); width:25px; text-align:center;'>";
					html += toStdString(TruthTableControl->Columns[i]->HeaderText);
					html += "</td>";
				 }
			 }
			 html += "</tr>";

			 html += in_html + "</table>";
			
			 return html;
		 }
		 
		 void TTSelectAll(System::Object^ sender, System::EventArgs^  e) {
			 TruthTableControl->SelectAll();
		 }
		 
		 void TTInvert(System::Object^ sender, System::EventArgs^  e) {
			 System::Windows::Forms::DataGridViewRowCollection^ rows = TruthTableControl->Rows;
			 for(int i = 0; i < rows->Count; i++) {
				 System::Windows::Forms::DataGridViewCellCollection^ cells = rows[i]->Cells;
				 for(int j = 0; j < cells->Count; j++) {
					 System::Windows::Forms::DataGridViewCell^ cell = cells[j];
					 cell->Selected = !cell->Selected;
				 }
			 }
		 }
		 
		 void TTSelectVars(System::Object^ sender, System::EventArgs^  e) {
			 System::Windows::Forms::DataGridViewRowCollection^ rows = TruthTableControl->Rows;

			 for(int i = 0; i < rows->Count; i++) {
				 System::Windows::Forms::DataGridViewCellCollection^ cells = rows[i]->Cells;

				 for(int j = 0; j < cells->Count; j++) {
					 System::Windows::Forms::DataGridViewCell^ cell = cells[j];
					 if(j<TruthTable.size()) {
						 cell->Selected=true;
					 } else {
						  cell->Selected=false;
					 }
				 }
			 }
		 }

		 void TTSelectFuncs(System::Object^ sender, System::EventArgs^  e) {
			 System::Windows::Forms::DataGridViewRowCollection^ rows = TruthTableControl->Rows;

			 for(int i = 0; i < rows->Count; i++) {
				 System::Windows::Forms::DataGridViewCellCollection^ cells = rows[i]->Cells;

				 for(int j = 0; j < cells->Count; j++) {
					 System::Windows::Forms::DataGridViewCell^ cell = cells[j];
					 if(j>=TruthTable.size()) {
						 cell->Selected=true;
					 } else {
						  cell->Selected=false;
					 }
				 }
			 }
		 }

		 void setHTMLClipboard(string htmlstr) {
			Encoding^ enc = Encoding::UTF8;

			String^ html = toSysString(htmlstr);

			String^ begin = "Version:0.9\r\nStartHTML:{0:000000}\r\nEndHTML:{1:000000}\r\nStartFragment:{2:000000}\r\nEndFragment:{3:000000}\r\n";
			String^ html_begin = "<html>\r\n<head>\r\n"
				+ "<meta http-equiv=\"Content-Type\""
				+ " content=\"text/html; charset=" + enc->WebName + "\">\r\n"
				+ "<title>HTML clipboard</title>\r\n</head>\r\n<body>\r\n"
				+ "<!--StartFragment-->";
			String^ html_end = "<!--EndFragment-->\r\n</body>\r\n</html>\r\n";
			String^ begin_sample = String::Format(begin, 0, 0, 0, 0);

			int count_begin = enc->GetByteCount(begin_sample);
			int count_html_begin = enc->GetByteCount(html_begin);
			int count_html = enc->GetByteCount(html);
			int count_html_end = enc->GetByteCount(html_end);

			String^ html_total = String::Format( begin
			, count_begin
			, count_begin + count_html_begin + count_html + count_html_end
			, count_begin + count_html_begin
			, count_begin + count_html_begin + count_html
			) + html_begin + html + html_end;

			DataObject^ obj = gcnew DataObject();
		  
			obj->SetData(DataFormats::Html, gcnew MemoryStream(enc->GetBytes(html_total)));
			Clipboard::SetDataObject(obj, true);
		 }
private: System::Void TruthTableControl_CellEndEdit(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
		 }
private: System::Void ResetTT_Click(System::Object^  sender, System::EventArgs^  e) {
			 ExpressionBox->Text = toSysString(Expression);
			 generateTruthTable();
		 }
private: System::Void ApplyTT_Click(System::Object^  sender, System::EventArgs^  e) {
			 string allEqs = "";

			 for(int i = 0; i < ChangedFuncs.size(); i++) {
				 string eq;

				 if(ChangedFuncs[i] == true) {
					 vector<bool> truthField;
					 int numTrue = 0;
					 int numFalse = 0;

					 for(int j = 0; j < FunctionVals[0].size(); j++) {
						 if(TruthTableControl[i+TruthTable.size(),j]->Value->ToString() == "0") {
							 truthField.push_back(false);
							 numFalse++;
						 } else {
							 truthField.push_back(true);
							 numTrue++;
						 }
					 }

					 truthField;

					 if(VarsMap.size() > 4) {
						 // go with SOPx or POSx;
						 kMap k1 = d1.generateKMap(TruthTable, VarsMap, truthField);
						 d1.solveKMap(k1);

						 if(k1.res == truthField) {
							 eq = k1.EPITerms;
						 } else {
							 if(numTrue>=numFalse) {
								 eq = d1.toPOSx(TruthTable, VarsMap, truthField);
							 } else {
								 eq = d1.toSOPx(TruthTable, VarsMap, truthField);
							 }
						 }
					 } else {
						 // go with minimization
						 kMap k1 = d1.generateKMap(TruthTable, VarsMap, truthField);
						 d1.solveKMap(k1);
						 eq = k1.EPITerms;
					 }

				 } else {
					 eq = Equations[i];
				 }
				
				 allEqs += eq + ";";
			 }

			 allEqs = allEqs.substr(0,allEqs.size()-1);
			 
			 ExpressionBox->Text = toSysString(allEqs);
			 generateTruthTable();
		 }

private: System::Void TruthTableControl_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
		 }
private: System::Void newToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			ExpressionBox->Text = "";
			
			LightBox->Hide();
			ChangedFuncs.clear();
			KTabs->TabPages->Clear();
			TruthTableControl->Columns->Clear();
			TruthTableControl->Rows->Clear();
			BulbsVector.clear();
			SwitchesVector.clear();
			DiagramsVector.clear();

			AND_Count.clear();
			OR_Count.clear();
			NOT_Count.clear();
			XOR_Count.clear();

			Status->Text = "New Project";
			this->Refresh();

			ExpressionBox->Focus();


		 }
private: System::Void saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			saveFileDialog1->Filter="DS files (*.dseq)|*.dseq|All files (*.*)|*.*";
			saveFileDialog1->DefaultExt=".dseq";
			saveFileDialog1->Title="Save DS Project";
			saveFileDialog1->FileName="DS-1.dseq";
			System::Windows::Forms::DialogResult^ result = saveFileDialog1->ShowDialog();
			String^restr = result->ToString();
			if (restr == "OK") {
				ofstream fstr(toStdString(saveFileDialog1->FileName));
				
				String^ theVersion = System::Reflection::Assembly::GetExecutingAssembly()->GetName()->Version->ToString();

				fstr << "Ver:" << toStdString(theVersion) << '\n';

				for(int i = 0; i < Equations.size(); i++) {
					fstr << "eq:" << Equations[i] << '\n';
				}

				fstr.close();
			}

		 }
private: System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			openFileDialog1->Filter="DS files (*.dseq)|*.dseq";
			openFileDialog1->DefaultExt=".dseq";
			openFileDialog1->Title="Open DS Project";
			openFileDialog1->FileName="";
			System::Windows::Forms::DialogResult^ result = openFileDialog1->ShowDialog();
			String^restr = result->ToString();
			if (restr == "OK") {
				ifstream fstr(toStdString(openFileDialog1->FileName));

				string str;
				string eqs = "";

				while(getline(fstr,str)) {
					if(str.find("eq:") == 0) {
						eqs += str.substr(3,str.length()-3) + ";";
					}
				}

				if(eqs.length()>0) {
					eqs = eqs.substr(0,eqs.length()-1);
				}

				ExpressionBox->Text = toSysString(eqs);
				generateTruthTable();

				fstr.close();
			}

		 }
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 Application::Exit();
		 }
private: System::Void homepageToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 System::Diagnostics::Process::Start("http://xelonlabs.com/DLD_Guide");
		 }
private: System::Void saveTruthTableToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			ClearExpressions();
			ExpressionBox->SelectedText="";
			ExpressionBox->Items->Clear();
		 }
private: System::Void copyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 TTCopy(sender, e);
		 }
private: System::Void saveToFileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 TTSave(sender, e);
		 }
private: System::Void countGatesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 int idx = KTabs->SelectedIndex;

			 string str = "Gates Report: \n";
			 str += "AND: " + toStdString(AND_Count[idx].ToString()) + "\n";
			 str += "OR : " + toStdString( OR_Count[idx].ToString()) + "\n";
			 str += "NOT: " + toStdString(NOT_Count[idx].ToString()) + "\n";
			 str += "XOR: " + toStdString(XOR_Count[idx].ToString()) + "\n\n";
			 str += "Total: " + toStdString((AND_Count[idx]+OR_Count[idx]+NOT_Count[idx]+XOR_Count[idx]).ToString());

			 MessageBox::Show(toSysString(str),"Gates Report");
		 }
private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			Form^ aForm = gcnew FrmAbout();
			aForm->ShowDialog();

		 }
private: System::Void TruthTableControl_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 int key = (int)e->KeyChar;

			 if(key != 48 && key != 49) {
				 return;
			 }
			 bool bKey = (key-48);
			 String^ sKey = (key-48).ToString();
			 
			 Color cHilight = Color::FromName("Highlight");
			 Color cText = Color::FromName("ControlText");
			 Color cLightText = TranslateColor(Color::FromName("Highlight"),Color::FromName("ControlLightLight"),0.3);
			 Color cGrey = Color::FromArgb(0,128,128,128);

			 for(int i = 0; i < TruthTableControl->Rows->Count; i++) {
				 for(int j = 0; j < TruthTableControl->Columns->Count; j++) {
					 if(TruthTableControl[j,i]->Selected==true && j >= VarsMap.size()) {
						 DataGridViewCell^ cell = TruthTableControl[j,i];
						 int funcIndex = j-VarsMap.size();
						 TruthTableControl[j,i]->Value = sKey;

						 if(bKey==true) {
							 if(cell->Style->ForeColor != cText) { cell->Style->ForeColor = cText; }
							 if(cell->Style->SelectionBackColor != cHilight) { cell->Style->SelectionBackColor = cHilight; }
						 } else {
							 if(cell->Style->ForeColor != cGrey) { cell->Style->ForeColor = cGrey; }
							 if(cell->Style->SelectionBackColor != cLightText) { cell->Style->SelectionBackColor = cLightText; }
						 }

						 if(ChangedFuncs[funcIndex] == false) {
							 TruthTableControl->Columns[j]->HeaderCell->Value += "*";
							 ChangedFuncs[funcIndex] = true;
						 }

					 }
				 }
			 }

			 LightBox->Show();

		 }
};
}

